#!/bin/env node

/*!
 * BrowserHunter - a browser detector
 * https://gitlab.com/mtroy/bhunter
 */

(function (window, undefined) {

	'use strict';

	const ALL = 'all'
	const NIX = 'nix'
	const DOS = 'dos'
	const WCE = 'wce'
	const CRO = 'cros'
	const MAC = 'mac'
	const IOS = 'ios'
	const AND = 'and'
	const SOL = 'sol'
	const WPH = 'wph'

	const RSC = 
	{
		OS:
		{
			/*
				m - ready pattern func
				p - matches position's
				e - entry point (first valid match)
				n - name
				v - version
				v - sub version / codename
				a - arch
				d - alternative device type (actually specified for ios)
			*/
			[NIX]: { p: { e: 2, n: 1, a: 2 }, m: (u)=>u.match(/((?:(?:x11|Wayland);\s)?(?:[\w]+;?\s){0,}?(?:(linux|\b[\w]+bsd\b(?: ([\d.]+);)?|dragonfly) (?=((?:x86_|aarch|amd|ppc|sparc)64|i[3456]86|armv[67]l)))|linux-gnu)/i) }, // (?<!msie [0-9.]+\b.+)
			[DOS]: { p: { e: 0, v: 0, a: 1 }, n: 'windows', m:(u)=> u.match(/(?:windows;(?!.+windows)|windows(?:\s(?:[\d\\.]+; (win 9x 4.90)|(?:nt\s?)?([\d\\.]+)?))[;\\)]\s?(?:((?:wow|win)64)|(?:win 9x 4.90\s)?(arm))?;?)/i) },
			[WCE]: { p: { e: 0, v: 0 }, n: 'windows mobile', a: 'arm', m:(u)=> u.match(/windows (?:(ce);|(ce [\d\\.]+))/i) },
			[CRO]: { p: { e: 1, a: 1, v: 2 }, n: 'chromeos', m:(u)=> u.match(/\b(cross?(?:os)?)\b (?=((?:x86_|aarch|amd|ppc|sparc)64|i[356]86|armv7l)? ([\d.]+);?)/i) },
			[MAC]: { p: { e: 0, a: 1, v: 2 }, n: 'mac', m:(u)=> u.match(/((?:macintosh|macbook air;)?(?:[\w]+;?\s){0,}((?:Intel|M1|PPC) Mac OS X) (?:([\d._]+);?\b)?)|cfnetwork\/.+darwin\/([\d.]+)|Mac_PowerPC/i) },
			[IOS]: { p: { e: 0, d: 0, v: 1 }, n: 'ios', m:(u)=> u.match(/(iphone|ipad|ipod|ipod touch); (?:(?:cpu(?:\siphone)? os)\s([\d._]+) like Mac OS X)?/i) },
			[AND]: { p: { e: 0, v: 1, vv: 2 }, n: 'android', m:(u)=> u.match(/(?<!windows (?:phone(?: os)?|mobile) [\d.]+;\s)(\bandroid;? (?:rv:|([\d.]+))?;?)(?:.*(HarmonyOS|kaios(?:\/[\d.]+)))?/i) },
			[WPH]: { p: { e: 0, v: 0 }, n: 'windows phone', m:(u)=> u.match(/(?:windows (?:phone(?: os)?|mobile))[/ ]?([\d.\w ]+)\b/i) },
			[SOL]: { p: { e: 0, v: 0 }, n: 'solaris', a: 'sparc', m:(u)=> u.match(/(?:sunos (?:([\d.]+) [\w\d]+|([\w.\d]+))|((?:open)?solaris)[-/ ]?([\w.]*))/i) }
		},
		DISTRIB: "(ubuntu|slackware|debian|linux\\smint|raspbian|centos|fedora|suse|opensuse|gentoo|archlinux|manjaro|zenwalk|sabayon|mandriva|pclinuxos|redhat|plan\\s9|minix)",
	    OSVERSION:
		{
		    [DOS]:
			[
			    // (?:(?:windows(?: (?:nt|98;))? ((?:(?:ce|iot) )?[\d.]+|win 9x 4.90);?\s?)((?:wow|win)64|arm)?)
			    ['win 9x 4.90', 'me'], ['95', '95'], ['98', '98'],
			    ['3.1', '3.1'], ['4.0', 'nt'], ["ce", "ce"],
			    ['5.0', '2000'], ['5.1', 'xp'], ['5.2', 'server 2003'],
			    ['6.0', 'vista'], ['6.1','7'],
			    ['6.2', { d: '8', a: 'rt(8)' }], ['6.3', { d: '8.1', a: 'rt(8.1)' }],
			    ['10.0', '10'], ['iot','iot']
			    // (Windows RT 10 Entreprise 1703) : Mozilla/5.0 (Windows NT 10.0; ARM) 
			],
		    [MAC]:
			[
			    ['17.0.0', '10.13'], ['17.2.0', '11.1'], ['17.6.0','11.4'],
			    ['17.7.0', '11.4.1'], ['18.0.0','12.0'], ['18.2.0','12.1'],
			    ['18.5.0', '12.2'], ['18.6.0', '12.3'], ['18.7.0', '12.4'],
			    ['19.0.0', '13.0'], ['19.2.0', '13.3'], ['19.3.0', '13.3.1'],
			    ['19.4.0', '13.4'], ['19.5.0', '13.5'], ['19.6.0', '13.6'],
			    ['20.0.0', '14.0'], ['20.1.0', '14.2'], ['20.2.0', '14.3'],
			    ['20.3.0', '14.4'], ['20.4.0', '14.5']
			    // 22.0.0 mac
			    // 22.1.0 mac
			    // 21.6.0 mac
			]
		},
	    BOT:
		{
			/*
				DirBuster ability
				IonCrawl
				 Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36 React.org
			*/
		    analyzer: ['locatetech.io', 'php version tracker', 'zgrab', 'InternetMeasurement', 'IonCrawl'],
		    userCollector: ['szukacze', 'opendi'],
		    seo: ['pulno', 'rogerbot', 'woobot', 'rankingbot2', 'freewebmonitoring', '403bot', 'loadtimebot', 'rankactivelinkbot', 'seobilitybot', 'socialrankiobot', 'dragonbot', 'seolizer'],
		    market: ['Pandalytics', 'seocompany', 'CheckMarkNetwork', 'pdrlabs.net', 'Dataprovider.com', 'applebot', 'contxbot', 'dotbot', 'AmazonAdbot', 'AmazonProductDiscovery', 'ahrefsbot', 'semrushbot', 'cincraw', 'queryseekerspider', 'serpanalytics', 'sproose', 'serpstatbot', 'aboutusbot', 'hypercrawl', 'domainsigmacrawler', 'twengabot-discover', 'tweetmemebot', 'yanga worldsearch', 'domaintunocrawler', 'uribot', 'es_bot', 'trendictionbot', 'x28-job-bot'],
		    archives: ['cdlwas_bot', 'nicecrawler', 'archive.org_bot'],
		    domain: ['npbot', 'safedns', 'nexcess', 'tcinet.ru'],
		    serp: ['bingbot', {'Baiduspider-render':'baiduspider', 'baiduspider':'baiduspider'}, 'FindCanBot', 'Mail.RU_Bot', 'SeznamBot', 'Exabot', 'DuckDuckBot', 'MJ12bot', 'Qwantify', 'holmes', { 'coccocbot-web': 'coccocbot' }, 'Google-Safety', '(yandex(bot|favicons|imageresize|direct|media|directfetcher|accessibilitybot)|yadirectfetcher)'],
		    rss: ['RSSMicro', 'xrss', 'feedspot', 'QuiteRss'],
		    adult: ['SmartCJ', 'SCJchecker'],
		    security: ['censysinspect', 'palo alto', 'security.ipip.net', { 'info@netcraft.com': 'NetcraftSurveyAgent', 'Netcraft Web Server Survey': 'NetcraftSurveyAgent' }],
		    sharing: ['facebookexternalhit'],
		    unknown: ['The Knowledge AI', 'zemeu', 'ALittle Client', 'chakal1337'],
		    parasite: ["masscan"]
		},
	    ENGINES:
		{
		    goanna: '(?<!gecko).+goanna/([\\d.]+)',
		    edgehtml: 'windows.+ edge/([1234][\\d]\\.[\\d.]+)',
		    blink: 'webkit/537\\.36.+chrome/(?![012][0123456]\\.)([0-9.]+)',
		    gecko: '(gecko)/([0-9.]+) (?!goanna)',
		    trident: '(trident)/([0-9.]+)\\b',
		    webkit: 'webkit/([0-9.]+)',
		    khtml: 'khtml/([0-9.]+)',
		    presto: 'presto/([0-9.]+)'
		    // servo: '',
		   	// dillo: '',
		   	// icab: '',
		   	// elektra: '',
		   	// ekiohflow: '',

		   	// (?!chrome/(?:[3456789]|1[\\d]{2}\\.))
		   	// .+(?!.+edge/([1234][\\d]\\.[\\d.]+))
		},
	    BROWSERS:
	    [
			/// - Mozilla/5.0: Previously used to indicate compatibility with the Mozilla rendering engine.
			// MozillaProductSlice. Claims to be a Mozilla based user agent, which is only true for Gecko browsers like Firefox and Netscape. For all other user agents it means 'Mozilla-compatible'. In modern browsers, this is only used for historical reasons. It has no real meaning anymore

			// -Security values:
			//  A single "U" (or "U;") in a user agent refers to the encryption strength that the browser supports.
			//     N for no security
			//     U for strong security (browser supports 128 bit encryption)
			//     I for weak security


			// - Mobile/7B405: This is used by the browser to indicate specific enhancements that are available directly in the browser or through third parties. An example of this is Microsoft Live Meeting which registers an extension so that the Live Meeting service knows if the software is already installed, which means it can provide a streamlined experience to joining meetings

			// -gecko/20100112
			// Build Date:
			// the date the browser was built 

	        /*
	         * [Brower name, 	[osallowed], [osdisallow], rule, versionFixer, osCompat]
			 */

	        // // yandex browser
	        ['Yandex Browser', 	[ALL], [], '(?:YaBrowser/([0-9.]+(?: \\(?:alpha|beta\\))?)) (?:.*Mobile)? (?:safari/[0-9.a-z]+)'],

	        /*
	        	(Based on Safari)	(Safari build)
	        	 		   Safari /	537.36
			*/
	        ['Chrome',			[IOS], [], '(?:crios/([0-9.a-z]+)).*Mobile(?:/[0-9a-z]+)? (?:Safari)(?:/[0-9.]+)?'],
	        ['Chrome',			[ALL], [], '(?<!wv\\))Chrome(?!book)(?:/([\\.\\d]+))? (?=(?:Mobile\\s)?Safari/[\\.\\d]+)'], // '(?:(?:(?:Gecko/[0-9.]+)).*)?(?:chrome/([0-9.]+)(?: safari/[0-9.a-z]+)?$)'],
	        ['Chrome',			[CRO], [], '(?:chrome/([0-9.]+)) (?:Safari/[0-9.]+)?'],
	        ['Chromium', 		[ALL], [], 'chromium[/\\s]([0-9.]+|gost)'],
	        ['Android webview',	[AND], [], 'wv\\).+chrome/([\\w.]+)'],

	        // Firefox
	        ['Firefox',			[ALL], [], '(?<!.*Opera[ /])Firefox(?:/(\\d+[\\.\\d]+))?(?!\\spalemoon)?'],
	        	// '(?:(?:(?:Gecko/[0-9.]+|fxios/[0-9a-z.])).*)?(?:(chrome)/([0-9.]+)(?: safari/[0-9.a-z]+)?$|(firefox)/([0-9.]+)(?:\\s[a-z]+os/[0-9.]+)?$)'],
	        ['Firefox',			[IOS], [], '(?:fxios/([0-9.a-z]+)).*Mobile(?:/[0-9a-z]+)? (?:Safari)(?:/[0-9.]+)?'],

	        ['Safari Mobile',	[IOS], [], '(?:iPod|iPad|iPhone).+(?:(?:(?<!(?:crios|edgios).+)Version|MobileSafari)\\/([.\\d]+)\\s(?!edgios).+safari\\/[\\d.]+|(?<!(?:crios|edgios).+)(?:Mobile.+(?=version))(?:version\\/([.\\d]+))? Safari\\/|version\\/([\\d.]+)\\s(?!edgios).+mobile\\/.+safari\\/[\\d.]+)'],
	        ['Safari', 			[ALL], [IOS, AND], 'Version/(\\d+[\\.\\d]+).*Safari/'],
			// (?<!crios/[0-9.a-z]+.*)(?:version/([0-9.]+))? Mobile(?:/[0-9a-z]+)?(?:.*version/([0-9.]+))? safari(?:/[0-9.]+)'],

	        // maxthon (first named MyIE2)
	        ['Maxthon', 		[ALL], [], '(?:Maxthon;?)/?([0-9.]+|(?: .NET CLR )[0-9.]+)?'],
	        ['Maxthon',			[IOS], [], '(?:mxios|maxthon)/([0-9.]+)'],
	        ['Maxthon',			[AND], [], '(?:maxthon \\(([0-9.]+)\\);|mxbrowser/([0-9.]+))'],

	        // IE
	        ['IE mobile', 		[WCE], [], 'IEMobile ([\\d\\.]+)'], // trident 3.1 ->  Mozilla/4.0 (compatible; MSIE 7.0; Windows Phone Os 7.0; Trident/3.1; IEMobile/7.0
	        ['IE mobile', 		[WPH], [], 'msie\\s([0-9.]+);'],

	       	// BEWARE OF QUIRKS MODE (and UA could be manually changed from the windows Registry...)
	        // https://web.archive.org/web/20141024073201/http://msdn.microsoft.com/en-US/library/ie/bg182625(v=vs.85).aspx
	        // https://web.archive.org/web/20141214190444/http://msdn.microsoft.com/en-us/library/ie/ms537503(v=vs.85).aspx
			// The compatible ("compatible") and browser ("MSIE") tokens have been removed.
			// The "like Gecko" token has been added (for consistency with other browsers).
			// The version of the browser is now reported by a new revision ("rv") token.
			// In rare cases, it may be necessary to uniquely identify IE11. Use the Trident token to do so.
	        ['IE', 				[DOS], [], 'MSIE.*(?<!.+maxthon)Trident/([4-7].[0-1])(?!.+maxthon)', [["3.1", "5.5"], ['4.0', '8.0'], ['5.0', '9.0'], ['6.0', '10.0'], ['7.0', '11.0'], ['8.0', '11.0']] ],
	        ['IE', 				[DOS], [], '(?:msie\\s([0-9.]+);(?!.+(?:maxthon|trident))|trident/?(?:[4-7].[0-1]);(?:.*rv:([0-9.]+).*like gecko))'],

			['Edge webview',	[DOS,WPH], [], 'MSAppHost/([0-9.]+).+Edge/([1234][\\d]\\.[\\d.]+)$'], // MSAppHost/<WebView Rev>" is added when EdgeHTML is hosted in a Universal Windows App using WebView.
			['EdgeHTML',		[DOS,WPH], [], '(?<!MSAppHost.+)Edge/([1234][\\d]\\.[\\d.]+)$'],
	        ['Edge',			[ALL], [], 'Edg(?:e|a)?\\/([789][\\d.]+|1[\\d]{2}\\.[\\d.]+)$|edgios\\/([789][\\d.]+|1[\\d]{2}\\.[\\d.]+)'],
	        ['Flow', 			[AND], [], '\\bFlow\\b'],

	        // opera
	        /*
				opera 9.80	  >>> old version, for compatiblity reasons has been frozen. See Version below Version 9.80 https://dev.opera.com/articles/opera-ua-string-changes/ | https://www.sitepoint.com/opera-10-user-agent/
				Opera -> OPR  >>> and Since July 15, 2013, opera UA begins with Mozilla/5.0 and to avoid encoutering legacy server rules, "opera" has been replaced by "OPR"
				Version/12.16 >>> Server version, the build number of the Opera Mini transcoder server
				https://dev.opera.com/blog/opera-14-beta-for-android-is-out/

				Before migrating to the Chromium code base, 
				Opera was the most widely used web browser that did not have the User-Agent string 
				with "Mozilla" (instead beginning it with "Opera"). 
				Since July 15, 2013,[7] Opera's User-Agent string begins with "Mozilla/5.0" and, 
				to avoid encountering legacy server rules, 
				no longer includes the word "Opera" (instead using the string "OPR" to denote the Opera version). 
	        */
	        ['Opera', 			[ALL], [], '(?:Opera(?! mini)[/ ]|OPR[/ ])(?:9.80.*Version/)?([\\.\\d]+)'],

	        // https://dev.opera.com/blog/opera-mobile-12-1-with-spdy-web-sockets-flexbox-and-more/
	        ['Opera Touch',		[AND], [], '\\b(?:opt)/([0-9.]+)'],
	        ['Opera mobile',	[AND], [], '\\b(?:opera|opr)/([0-9.]+)(?:.+opera mobi)?'],
	        ['Opera mini', 		[AND, IOS], [], '\\bopera ?mini/(?:att/)?([\\.\\d]+)'],
	        ['Opera mobile',	[IOS], [], '\\bopios/([\\.\\d]+)'],
	        // (?:Opera Tablet.*Version|Opera/.+Opera Mobi.+Version|Mobile.+OPR)/(\\d+[\\.\\d]+)
	        // Opera/(\\d+[\\.\\d]+).+Opera Mobi

	        
	        ['baidu spark',		[ALL], [], '(?:baidu )?spark(?:\\sbrowser\\s(?:for\\sandroid\\s)?|\\/)([0-9.]+x?)'],
	        ['baidu browser',	[DOS,AND,IOS], [], '(?:bdbrowser|baidubrowser)/([0-9.]+)'],
			['baidu browser',	[DOS], [], 'BIDUBrowser/([0-9.]+x?)'],
			['baidu boxapp',	[AND,IOS], [], 'baiduboxapp/([0-9.]+)'],
			['baidu HD',		[IOS], [], 'BaiduHD/([0-9.]+)'],
			['Iceweasel',		[DOS,NIX], [], 'Iceweasel/([0-9.]+)'],
	        ['NTENT Browser',	[AND,NIX], [], 'NTENTBrowser/([0-9.]+)'], // https://ntent.com/mobile-browser/ 
	        ['Venus Browser',	[AND], [], 'VenusBrowser/([0-9.]+)'],
			['Surf Browser',	[AND,IOS], [], 'SurfBrowser/([0-9.]+)'],
			['Jio Browser',		[AND], [], 'JioBrowser/(1[0-9.]+)|JioPages/([0-9.]+)'], // last rev on v1 is 1.4.7, next uses 'jiopages'
			['Arora',			[DOS,MAC,NIX], [], 'Arora/([0-9.]+)'], // last ver 0.11.0
			['QQBrowser',		[AND,DOS,MAC,IOS], [], 'M?QQBrowser(?:Lite)?/([0-9.]+)'],
			['DuckDuckGo',		[ALL], [], 'DuckDuckGo/([0-9]+)'],
			['SputnikBrowser',	[ALL], [], 'SputnikBrowser/([0-9.]+)(?: \\(gost\\))?(?: safari)'],
			['Waterfox',		[ALL], [], 'Waterfox/([0-9.]+)'],
			['RockMelt',		[ALL], [], 'RockMelt/([0-9.]+)'],
	        ['Samsung browser',	[AND], [], 'SamsungBrowser/([0-9.]+)'],
	        ['Miui Browser',	[AND], [], 'XiaoMi/MiuiBrowser/([0-9.]+)'],
	        ['Brave',			[ALL], [], '(?<!chrome\\/[0-9.]+\\s)brave(?:\\/([0-9.]+))?', [["brave", "???"]]], //'brave(?:(?:\\/([0-9.]+))? (?!chrome)|\\schrome\\/([\\d.]+))'],
	        ['UC Browser',		[ALL], [], 'ucbrowser/([0-9.]+)'],
	        ['Sleipnir',		[ALL], [], 'Sleipnir[0-9]{1}?/([0-9.]+)'],
	        ['Huawei Browser',	[ALL], [], 'huaweibrowser/([0-9.]+)'],
	        ['Palemoon', 		[ALL], [], 'palemoon/([0-9.]+)'],
	        ['Vivaldi', 		[ALL], [], 'vivaldi/([0-9.]+)'],
	    	['SeaMonkey', 		[ALL], [], 'seamonkey/([0-9.a-z]+)'],
	    	['Konqueror', 		[NIX], [], 'konqueror/([0-9.]+)'],
	    	['Conkeror', 		[NIX], [], 'conkeror/([0-9.]+)'],
	    	['Midori', 			[NIX, CRO, DOS, MAC], [], 'midori[/\\s]([0-9.]+)'],
	    	['Lynx', 			[NIX, CRO], [], '\\blynx/([0-9.]+)'],
	    	['Links',			[NIX], [], '\\blinks(?: \\((\\d+[\\.\\d]+))?'],
	    	['Netscape',		[DOS,MAC], [], 'netscape/([0-9.]+)']
	    ],
	    BROWSERSVERSION:
	    {
		    [DOS]:
			[
				// EdgeHTML / Edge
				["12.10240", "20.10240.16384.0"],
				["13.10586", "25.10586.0.0"],
				["14.14393", "38.14393.0.0"],
				["15.15063", "40.15063.0.0"],
				["16.16299", "41.16299.15.0"],
				["17.17134", "42.17134.1.0"],
				["18.17763", "44.17763.1.0"],
				["18.17763", "44.17763.1.0"],
				["18.18362", "44.18362.1.0"],
				["18.18363", "44.18362.387.0"],
				["18.19041", "44.19041.1.0"]
				// next gen begin with 79+ chromium based
			]
	    }
	}

	const INF = 
	{
		OS:
		{
			"windows":
			{
				"window":["g_opr"]
			},
			"ios":
			{
				fn:
				{

				}
			}
			// >>>>>>>> WINDOWS
			// {"g_opr":{ // WINDOWS ONLY



			// >>>>>>> IOS
				// "__Webkit-touch-callout" ---> ios
				// "__Webkit-overflow-scrolling":

				// for chrome based
				// https://softwareengineering.stackexchange.com/questions/198375/why-is-it-impossible-for-google-to-port-v8-along-with-chromes-codebase-in-c-obj
				// if(window.chrome && !Intl.v8BreakIterator)
				// 	-> its ios or mac



			// >>>>>>>>>>> MAC
			// "mac":
			// [
			// 	"__Moz-osx-font-smoothing":()=>
			// 	{
			// 		return ((typeof CSS !== "undefined" && typeof CSS.supports !== "undefined") &&
			// 			CSS.supports("-moz-osx-font-smoothing", "auto"))
			// 	}
			// }
						// "document.oncontentvisibilityautostatechange":
						// [
						// 	"opera win", // opera linux ---> false
						// 	"opera mobile",
						// 	"chrome win", // chrome linux and android ---> false
						// 	"edge android" // edge win ---> false
						// 	//windows
						// // result = [...chromenux.reduce((r, o) => {
						// //   r.delete(o);
						// //   return r;
						// // }, new Set(chromewin))];
						// ],
						// "window.defaultStatus":["linux, android"],
						// "window.defaultstatus":
						// [
						// 	"chrome linux", // chrome win --> false
						// 	"chrome android",
						// 	"opera linux", // opera mobile + opera win ---> false
						// 	"edge win" // edge mobile ---> false
						// ],
			// "window.chrome.csi":
			// [
				// "chrome":">=1"
			// 	"chrome win",
			// 	"chrome mobile",
			// 	"chrome linux",
			// 	"opera win", // opera mobile ---> false
			// 	"edge win",
			// 	"edge android"
			// ],

			// CHROME Win + chrome nux + opera nux + opera win + edge win
			// launchQueue


			// only mobile
				// 2: "onorientationchange"
				// 3: "orientation"
			// 4: "ontouchcancel"
			// 5: "ontouchend"
			// 6: "ontouchmove"
			// 7: "ontouchstart"

				// FIREFOX ALL
			//  	0: "getDefaultComputedStyle"
			// 1: "scrollByLines"
			// 2: "scrollByPages"
			// 3: "sizeToContent"
			// 4: "updateCommands"
			// 5: "dump"
			// 6: "setResizable"
			// 7: "event"
			// 8: "mozInnerScreenX"
			// 9: "mozInnerScreenY"
			// 10: "scrollMaxX"
			// 11: "scrollMaxY"
			// 12: "fullScreen"
			// 13: "ondevicemotion"
			// 14: "ondeviceorientation"
			// 15: "onabsolutedeviceorientation"
			// 16: "InstallTrigger"
			// 17: "sidebar"
			// 18: "ondragexit"
			// 19: "onloadend"
				// 20: "onmozfullscreenchange"
			// 21: "onmozfullscreenerror"
			// 22: "onanimationcancel"
			// 23: "ongamepadconnected"
			// 24: "ongamepaddisconnected"
			// 25: "caches"

			// FIREFOX Win
			// 	"sidebar"
			// 	caches ???

			// FIREFOX DROID
			// has no window.print

			// OPERA DROID + OPERA NUX
			// has no window.speechSynthesis
				// but opera win is true
		},

		BROWSERS_DISCOVER:
		{
			ie:()=>
			{
				let bVer = undefined
				/////// ie
				if(
					(
						/*@cc_on!@*/false || // <= 10
						document.createEventObject // <= ie8
					)
					&& typeof window.toStaticHTML !== "undefined"
				)
				{
					// Conditional Compilation to test the JavaScript version
					if(!!(Function('/*@cc_on return (@_jscript_version == 5.6 || (@_jscript_version == 5.7 && /MSIE 6\.0/i.test(navigator.userAgent))); @*/')())) { bVer = "=6" }
					if(!!(Function('/*@cc_on return (@_jscript_version == 5.7 && /MSIE 7\.0(?!.*IEMobile)/i.test(navigator.userAgent)); @*/')())) { bVer = "=7" }
					if(!!(Function('/*@cc_on return (@_jscript_version > 5.7 && !/^(9|10)/.test(@_jscript_version)); @*/')())) { bVer = "=8" }
					if(!!(Function('/*@cc_on return (/^9/.test(@_jscript_version) && /MSIE 9\.0(?!.*IEMobile)/i.test(navigator.userAgent)); @*/')())) { bVer = "=9" }
					if(!!(Function('/*@cc_on return document.documentMode===10@*/')())) { bVer = "=10" }
				}
				return bVer
			},

			//////// chrome like
			chrome:()=>
			{
				// chrome object
				/*
					chrome ≥ 1
					opera ≥ 14
					android ≥ 4.0.4
				*/
				// https://softwareengineering.stackexchange.com/questions/198375/why-is-it-impossible-for-google-to-port-v8-along-with-chromes-codebase-in-c-obj
				// if(window.chrome && !Intl.v8BreakIterator)
				if(typeof window.chrome !== "undefined" && Intl.v8BreakIterator &&
				(
					typeof window.opr === "undefined" && 
					typeof window.operamini === "undefined" &&
					typeof navigator.brave === "undefined"
				)){ return ">=1" }
			},

			//////// opera
			opera:()=>
			{
				let bVer = undefined
				if(typeof window.operamini !== "undefined" && Object.prototype.toString(window.operamini) == "[object operamini]") { bName = "opera mini" }
				if(typeof window.opera !== "undefined" && Object.prototype.toString(window.opera) == "[object opera]" && typeof window.history.navigationMode !== "undefined") { bVer = [">=5","<=14"] }
				if(typeof window.opr !== "undefined") { bVer = ">=15"  } // // window.opr.addons.installExtension
				return bVer
			},

			/////// brave
			brave:()=>
			{
				if(typeof navigator.brave !== "undefined" && typeof navigator.brave.isBrave !== "undefined") { return ">=0" }
			},

			firefox:()=>
			{
				if(typeof InstallTrigger !== "undefined") { return ">=1.5" }
			},
			"firefox mobile":()=>
			{
				if(typeof window.print === "undefined" &&
					typeof InstallTrigger !== "undefined")
					{ return ">=1" }
			}


			// if (document.getElementById) { 
			//   // Navigateur Netscape 5 et plus et DOM-1.
			// } 
			// else if (document.layers) { 
			//   // Navigateur Netscape 4.7 et moins. 
			// } 
			// else if (document.all) { 
			//   // Internet Explorer 
			// } 

					  // var safariVersion = 1;
			//     if (engine.webkit < 100) {
			//       safariVersion = 1;
			//     } else if (engine.webkit < 312) {
			//       safariVersion = 1.2;
			//     } else if (engine.webkit < 412) {
			//       safariVersion = 1.3;
			//     } else {
			//       safariVersion = 2;
			//     }

		},

		BROWSERS_LOCK:
		{
			"chrome.csi":{
				"chrome":">=1", // chrome.csi().pageT
				//https://chromereleases.googleblog.com/search?updated-max=2010-08-04T17:26:00-07:00&max-results=7&start=26&by-date=false
			},
				// "chrome.action":
				// {
				// 	"chrome":">=88"
				// 	// https://developer.chrome.com/docs/extensions/reference/
				// },
				// "chrome.audio":
				// {
				// 	"chrome":">=59"
				// },
				// "chrome.certificateProvider":
				// {
				// 	"chrome":">=46"
				// },
			"webkitSpeechGrammar":{
				"chrome":">=41"
			},
			"CSPViolationReportBody":{ // ??????????????????????
				"chrome":">=110"
			},

			"navigator.brave.isBrave":{
				"brave":[">=1", {unique:true}]
			},

  			"globalStorage":{
  				"firefox":[">=2","<=13"]
  			},
  			// "mozInnerScreenY":{
  			// 	"firefox":""
  			// },
			"InstallTrigger":{
				"firefox":[">=1.5", {unique:true}],
			},
			"sidebar":{
				// https://developer.mozilla.org/en-US/docs/Web/API/Window/sidebar
				"firefox":"<=102"
			},
			"SidebarMemexInputElement":{
				"firefox":">=106" /// to check
			},
			"navigator.requestMIDIAccess":{
				"firefox":">=108"
			},
			"navigator.storage":{
				// https://webkit.org/blog/12257/the-file-system-access-api-with-origin-private-file-system/
				"safari":">=12.1",
				"safari mobile": "=12.2.2"
			},
			"navigator.msMaxTouchPoints":{ // OK
				"ie":">=10"
			},
			"navigator.getGamepads":{
				"safari":">=10.1"
			},
			"navigator.mediaDevices":{
				"safari":">=11",
				"safari mobile":">=11"
			},

			// https://contest-server.cs.uchicago.edu/ref/JavaScript/developer.mozilla.org/en-US/docs/Web/API/Navigator/registerContentHandler.html
			"navigator.registerContentHandler":{
				"firefox":[">=2", "<=62", {unique:true}]
			},

			// check reliability
			"navigator.requestMediaKeySystemAccess":{ // not reliable due to https requirement
				"safari":">=12.1",
				"safari mobile":">=12.2",
				// "chrome":">=42", // [need https]
				// "opera":">=29",
				// "firefox":">=38", // [no need https]
				// firefox android ok
				// "edge":">=13",
				// "opera mobile":">=72",
				"samsung":">=4",
				"QQBrowser":">=13.1",
				"BaiduBrowser":">=13.18"
			},
			// "navigator.credentials":{ // Secure context ONLY
			// 	"safari":">=13",
			// 	"safari mobile":">=13",
			// 	"chrome":">=51"
			// },
			"navigator.mediaCapabilities":{
				"safari":">=13"
			},
			"navigator.mediaSession":{
				"safari":">=15",
				"safari mobile":">=15"
			},
			"navigator.storage":{
				"safari":">=15.2",
				"safari mobile":">=15.2"
			},
			"navigator.permissions":{
				"safari":">=16",
				"safari mobile":">=16",
				"opera mini":"<=66"
			},
			"navigator.getGamepads":{
				"safari mobile":">=10.3"
			},
			"document.layers":{
				"netscape":"<6"
			},
			"navigator.msLaunchUri":{
				// https://runebook.dev/it/docs/dom/navigator/mslaunchuri
				"edge":"<=18",
				"ie":"=11" // OK
			},
			"navigator.msSaveBlob":{
				// https://ix23.com/software-development/web-development/navigator/#navigatormsLaunchUri
				"edge":"=13",
				"ie":">=10" // OK (tested)
			},
			"document.documentMode":{
				// "edge":">=20",
				//    __        ____  ____________
				//   / /_____  / __ \/ ____/ ____/
				//  / __/ __ \/ / / / __/ / /_
				// / /_/ /_/ / /_/ / /___/ __/
				// \__/\____/_____/_____/_/
				
				"ie":">=8" // ???
			},
			"document.addEventListener":{
				"ie":">=9" // ???
			},
			"document.webkitmouseforcechanged":{
				// "safari mobile":"=9"
			},
			"webkitAudioContext":{
				"safari mobile":">=6"
			},

			"webkitStorageInfo":
			{

				// chrome but no firefox
				// https://groups.google.com/a/chromium.org/g/blink-dev/c/HPuDXPKwW_s?pli=1
				// window.webkitStorageInfo.PERSISTENT (say it is deprecated)
			},

			"SpeechSynthesisUtterance":{
				"safari mobile":">=7"
			},
			"indexedDB":{
				"ie":">=10", // OK (tested)
				"safari":">=7",
				"safari mobile":">=8"
			},

			"MSCSSMatrix":{
				"ie":[">=10","<=11"] // ???
			},
			"applicationCache": // require secure context
			{
				"ie":">=10" // OK (tested)

				// "chrome":"<=94"
				// https://web.dev/appcache-removal/
				// https://chromestatus.com/feature/6192449487634432
				// https://zditect.com/blog/1672051.html

				// safari ???
				// https://bugs.webkit.org/show_bug.cgi?id=181778

				// firefox
				// "firefox":"<=107"  /// STILL NEED SECURE CONTEXT
				// https://bugzilla.mozilla.org/show_bug.cgi?id=1237782
			},
			// 
				// https://web.dev/appcache-removal/
				// https://zditect.com/blog/1672051.html


			// window.webContents -> electron app
			// "window.Worker"


			"showModalDialog":{
				// https://blog.chromium.org/2014/07/disabling-showmodaldialog.html
				// https://udn.realityripple.com/docs/Web/API/Window/showModalDialog
				"firefox": "<56",
				"chrome":"<43",
				"safari":">=5.1"
				
			},
			"CSS.supports":
			{
				"edge":">=12",
				"opera":">=12.1",
				"firefox":">=22",
				"chrome":">=28",
				"safari":">=9",
				"safari mobile":">=9",
				"samsung":">=4"
			},
			"supportsCSS":{
				"opera":[">=12.10", "<15"]
				// https://dev.opera.com/articles/native-css-feature-detection/
				// https://dev.opera.com/blog/why-use-supports-instead-of-modernizr/
				// (it uses old syntax window.supportsCSS)
				// CSS.supports() was introduced in Blink once Google switched
				// on Chrome 28. It's of course, the same Blink used in Opera.
			},
			"media":{
				"ie":">=8" // ???
			},
			"matchMedia":{
				// https://udn.realityripple.com/docs/Web/API/Window/matchMedia
				"ie":">=10", // ???
				"safari mobile":">=5"
			},
			"StyleMedia":
			{
				"ie":">=9" // OK (tested)
			},
			"XDomainRequest":{
				"ie":[">=8", "<=9", {unique:true}] // ????
			},
			"XMLHttpRequest":{
				"ie":">=6" // OK (tested)
			},
			
			"MSStream":{
				// https://learn.microsoft.com/en-us/previous-versions/hh772328(v=vs.85)
				"ie":[">=10", {unique:true}] // OK (tested)
			},

			"msWriteProfilerMark":{
				// https://docs.w3cub.com/dom/mswriteprofilermark
				// https://webplatform.github.io/docs/apis/timing/methods/msWriteProfilerMark/
				// object.msWriteProfilerMark ????
				// !document.documentMode && window.msWriteProfilerMark
				"ie":[">=9", "<=11", {unique:true}], // ???
				// "edge":">=20"
			},

			// '-ms-scroll-limit' in document.documentElement.style 
			// && '-ms-ime-align' in document.documentElement.style 
			// && !window.navigator.msPointerEnabled;



			"chrome.webstore":{
				"chrome":[">=14", "<71"], // https://blog.chromium.org/2018/06/improving-extension-transparency-for.html
				"opera":"<15" // http://blogs.opera.com/desktop/2013/05/opera-next-15-0-released/
			},
			"attachEvent":{
				// https://dev.opera.com/blog/window-event-attachevent-detachevent-script-onreadystatechange/
				// https://stackoverflow.com/questions/13575732/why-is-document-attachevent-false-in-opera
				"opera":"<=11.60", 

				// attachEvent was a jScript command equivalent to addEventListener in JavaScript.
				// The only browsers to fully support JScript were IE3 through IE8.
				// IE9 had some partial support for jScript but also supported JavaScript.
				// IE10 and IE11 only support JavaScript and no jScript at all.

				// https://web.archive.org/web/20141027151001/http://msdn.microsoft.com/en-us/library/ie/ms536343(v=vs.85).aspx
				"ie":"<10" // OK (tested)
			},

			"opera":{
				// https://dev.opera.com/blog/300-million-users-and-move-to-webkit/
				// https://www.howtocreate.co.uk/operaStuff/operaObject.html
				// This is a script object that Opera has provided since Opera 5. It is provided to aid debugging of scripts.
				// Object.prototype.toString.call(window.opera) == '[object Opera]';  v10
				"opera":[">=5", "<=14", {unique:true}] // since Blink + V8
				
			},
			"g_opr":{ // WINDOWS ONLY
				"opera":[">=92", {unique:true, trustedOS:["windows"]}] // opera mobile + linux has not..
			},
			"opr":{
				"opera":[">=15", {unique:true}] // opera mobile has not...
			},
			// "opr.addons"  >= 20 ?? but not fund in 2023..
			"btoa":{
				"ie":">=10"
			},

			//????????????????????????????????? CSS.supports
			/*
				https://caniuse.com/?search=CSS.support
				chrome >= 28
				firefox >= 22
				safari >= 9
				safari mobile >= 9
				edge >= 12
				samsung >= 4
				opera >= 15
				opera mobile >= 73
			*/
			"__Webkit-touch-callout": // https://caniuse.com/?search=-webkit-touch-callout
			{
				bw:{
					"safari mobile":[">=3.2", {unique:true, trustedOS:["ios"]}]
				},
				fn:()=>
				{
					return ((typeof CSS !== "undefined" && typeof CSS.supports !== "undefined") &&
						CSS.supports("-webkit-touch-callout", "none"))
				}
			},

			// https://caniuse.com/mdn-css_properties_-webkit-mask-attachment
			"__Webkit-mask-attachment":
			{
				bw:{
					"chrome":[">=4", "<=23"],
					"safari":[">=4", "<=6.1"],
					"safari mobile":[">=3.2", "<=6.1"],
					"android browser":[">=2.1", "<=4.3"]
				},
				fn:()=>
				{
					return ((typeof CSS !== "undefined" && typeof CSS.supports !== "undefined") &&
						CSS.supports("-webkit-mask-attachment", "fixed"))
				}
			},

			// https://caniuse.com/mdn-css_properties_-webkit-overflow-scrolling
			"__Webkit-overflow-scrolling":
			{
				bw:{
					"safari mobile":[">=5", "<=12.5", {unique:true, trustedOS:["ios"]}]
				},
				fn:()=>
				{
					return ((typeof CSS !== "undefined" && typeof CSS.supports !== "undefined") &&
						CSS.supports("-webkit-overflow-scrolling", "auto"))
				}
			},


			// "__MS-scroll-chaining":
			// {
			// 	bw:{

			// 	},
			// 	fn:()=>
			// 	{

			// 		return ((typeof CSS !== "undefined" && typeof CSS.supports !== "undefined") &&
			// 			CSS.supports("-ms-scroll-chaining", "none"))
			// 	}
			// },
			"__TextCombineUpRight": // ???
			{
				bw:{
					"edge":[">=12","<=14"]
				},
				fn:()=>
				{
					return ((typeof CSS !== "undefined" && typeof CSS.supports !== "undefined") &&
						CSS.supports("text-combine-upright", "auto"))
					
				}
			},
			"__MSTextCombineUpRight": // ???
			{
				bw:{
					"edge":[">=15","<=18"],
					"ie":"=11"
				},
				fn:()=>
				{
					return ((typeof CSS !== "undefined" && typeof CSS.supports !== "undefined") &&
						CSS.supports("-ms-text-combine-horizontal", "auto")) // or digits
					
				}
			},
			
			"__jsCompil": // OK
			{
				bw:{
					"ie":[">=6", "<=10"]
				},
				fn:()=> /*@cc_on!@*/false
			},

			"__Canvas":
			{
				bw:
				{
					"opera":">=10",
					"ie":">=9"
				},
				// https://www.sitepoint.com/canvas-support-ie9/
				// https://www.aspsnippets.com/Articles/Excanvas-Example-Using-HTML5-Canvas-in-Internet-Explorer-versions-7-and-8-IE7-and-IE8-using-Excanvas.aspx
				// http://diveintohtml5.info/canvas.html
				fn:()=>
				{
				return typeof document !== "undefined"
					? typeof document.createElement("canvas").getContext !== "undefined"
					: false
				}
			},
			"__Video":
			{
				bw:
				{

					"opera":">=10.5",
					"ie":">=9"
				},
				fn:()=>
				{
					// ie9 support this format in its final version
					// try H.264 video + AAC audio in an MP4 container
					// [ .canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"')].includes("probably");

					return typeof document !== "undefined"
						? typeof document.createElement('video').canPlayType !== "undefined"
						: false
					// return !!(v.canPlayType && v.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"').replace(/no/, ''));
				}
			},
			"__HLSappleLive":
			{
				bw:{

				},
				fn:()=>
				{
					const v = typeof document !== "undefined" && document.createElement('video');
					return (typeof v.canPlayType !== "undefined" && 
						v.canPlayType('application/vnd.apple.mpegURL')
						.replace(/no/, ''));
				}
			},
			"__H264":
			{
				bw:
				{
					"chrome":">=4",
					"safari":">=3.2",
					"firefox":">=35",
					"ie":[">=9","<=11"],
					"opera mobile":">=73",
					// "opera":">=24.0.1558.3" // https://blogs.opera.com/desktop/changelog-24/
					// https://forums.opera.com/topic/34659/opera-linux-browser-h-264-support-through-x264-open-source-codec/25
					// CHR-2543 Implementation of MP3 and H.264 support in Windows
					// CHR-2604 Initial support for H.264 decoding on OSX
					// "opera":">=24.0.1555.0" 
				},
				fn:()=>
				{
					const v = typeof document !== "undefined" && document.createElement('video');
					return (typeof v.canPlayType !== "undefined" && 
						v.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"')
						.replace(/no/, ''));
				}

					// “probably”: The browser most likely supports this video type.
					// “maybe”: The browser might support this video type.
					// “” (empty string): The browser does not support this video type.

					// ogg 'video/ogg; codecs="theora, vorbis"'
					// h264 'video/mp4; codecs="avc1.4D401E, mp4a.40.2"'
					// webm 'video/webm; codecs="vp8.0, vorbis"'
			},
			"__ComputedGetter":
			{
				bw:{
					"chrome":">=46",
					"edge":">=12",
					"firefox":">=34",
					"opera":">=47",
					"safari":">=9.1",
					"chrome android":">=46",
					"firefox android":">=34",
					"opera android":">=33",
					"safari ios":">=9.3",
					"samsung":">=5"
				},
				fn:()=>
				{
					try{
						var expr = "foo";
						var obj = {
							get [expr]() {
								return "bar";
							},
						};
						return true;
					}catch(e)
					{
						return false;
					}
				}
			},
			"__EdgeStyles":
			{
				bw:{
					"edge":[">=12"]
				},
				fn:()=>
				{
					// https://github.com/4ae9b8/browserhacks/issues/167
					return ((typeof document !== "undefined" &&
						     typeof document.documentElement !== "undefined")
					
					&& ('-ms-scroll-limit' in document.documentElement.style &&
					    '-ms-ime-align' in document.documentElement.style)

					&& (typeof window.navigator !== "undefined" &&
					    typeof window.navigator.msPointerEnabled === "undefined"))

					// v12-13
					// -ms-accelerator:true

					// v12+
					// -ms-ime-align:auto >= 12

					// v????
					// '-ms-scroll-limit' in document.documentElement.style 
					// && '-ms-ime-align' in document.documentElement.style 
					// && !window.navigator.msPointerEnabled;
				}
			},
			"__WebP":
			{
				bw:{
					"ie":">=9",
					"firefox":">=65",
					"chrome":">=9",
					"edge":">=18",
					"safari":">=14",
					"safari mobile":">=14",
					"opera":">=11.5",
					"opera mobile":">=12",
					"chrome android":">=78",
					"firefox android":">=68",
					"android browser":">=4",
					"ucbrowser":">=12.12",
					"samsung":">=4",
					"qqbrowser":">=1.2",
					"baidubrowser":">=7"

				},
				fn:()=>
				{
					// using Pormise

					// function checkWebPSupport) {
					//   return new Promise((resolve, reject) => {
					// 	var img = new Image();
					// 	img.onload = function() { resolve(); };
					//     img.onerror = function() { reject(); };
					//     img.src = 'http://www.gstatic.com/webp/gallery/1.webp';
					//   })
					// }

					// using CANVAS
					if(typeof document !== "undefined")
					{
						const e = document.createElement('canvas');
						if(e.getContext !== "undefined")
						{
							if(e.getContext('2d'))
							{
								return e.toDataURL('image/webp').indexOf('data:image/webp') === 0;
							}
						}
					}
					return false;

				}
			},
			"__SafNotif":
			{
				bw:{
					"safari":[">=10", {trustedOS:["mac"]}], 
					"safari mobile":[">=10", {trustedOS:["ios"]}]
				},
				fn:()=>
				{
					if(typeof window !== "undefined")
					{
						(function (p)
						{
						    return p.toString() === '[object SafariRemoteNotification]'
						})(
							typeof window.safari === "undefined" ||
							(typeof safari !== 'undefined' && window.safari.pushNotification)
						)
					}else{ return false; }
				}
			},
			"__SafConstruct":
			{
				bw:{
					"safari":["<=9.1.3", {trustedOS:["mac"]}], 
					"safari mobile":["<=9.1.3", {trustedOS:["ios"]}]
				},
				fn:()=> (typeof window !== "undefined" && /constructor/i.test(window.HTMLElement))
			},
			"__SafAudioQuirk":
			{
				bw:{"safari mobile":["<=12",{trustedOS:["ios"]}]},
				fn:()=>
				{
					// volume cannot be changed from "1"
					if(typeof Audio !== "undefined")
					{
				        var audio = new Audio();

				        audio.volume = 0.5;
				        return audio.volume === 1;
				    }
				    return false
				}
			},
			"__SafInlineVideo":
			{
				bw:{"safari mobile":[">=10",{trustedOS:["ios"]}]},
				fn:()=>
				{
					// https://github.com/Modernizr/Modernizr/issues/2077
					if(typeof document !== "undefined")
					{
						return document.createElement('video').playsInline !== "undefined";
					}
					return false
				}
			},
			// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/toString
			// "__FuncToString":
			// {
			// 	bw:
			// 	{
			// 		"firefox":true
			// 	}
			// },
			"__CallStack":
			{
				bw:
				{
					"chrome":">=1",
					"edge":">=18",
					"edge mobile":">=18",
					"opera":">=15",
					"opera mobile":"=12",
					"opera mobile":">=73",
					"firefox":">=1"
				},
				fn:(tk)=>
				{
					const b = tk?.bw?.name || ""
					try
					{ 
						Function(`(function(){"use strict";return(function f(n){if(n<=0){return "foo";}return f(n - 1);}(1e6))==="foo";})()`)() 
					}catch(e)
					{
						let r = e.toString()
						// if(typeof document !== "undefined")
						// {
						// 	document.write("<br>STACK: ["+r+"]")
						// }
						switch(b)
						{
							case "chrome":
							case "edge mobile":
							case "opera mobile":
							case "opera":
								return new RegExp("RangeError: Maximum call stack size exceeded", 'i').test(r)
							case "firefox":
								return new RegExp("InternalError: too much recursion", 'i').test(r)
							// case "opera":
							// 	return new RegExp("EvalError: Refused to evaluate a string", 'i').test(r)
						}
						return false
					}
				}
			},
			"__Object.observe":
			{
				// https://simpl.info/observe/
				// https://caniuse.com/object-observe
				// https://chromestatus.com/feature/6147094632988672
				bw:{
					"chrome":[">=36", "<50"],
					"opera":[">=23", "<37"],
					"samsung":"=4"
				},
				fn:()=>
				{
					return typeof Object.observe !== "undefined"
				}
			},
			"__Object.Spread":
			{
				bw:{
					"chrome":">=60",
					"firefox":">=55",
					"opera":">=47",
					"edge":">=79",
					"safari":">=11.1"
					// node 8.3
				},
				fn:()=>
				{
					try {
						new Function("obj = {...{test:true}}");
					}catch(err){
						return false;
					}
					return true;
				}
			},
			"__Object.Assign":
			{
				bw:{
					"chrome":">=45",
					"firefox":">=34",
					"opera":">=32",
					"edge":">=12",
					"safari":">=9",
					// node 4
				},
				fn:()=>
				{
					return typeof Object.assign !== "undefined"
				}
			},
			"__ErrorLine":{
				bw:{
					"firefox":">=1",
					"firefox mobile":">=4"
				},
				fn:()=>
				{
					try { throw new Error("Could \not parse input"); } 
					catch (err) {
						return err.hasOwnProperty("lineNumber")
					}
				}
			}
			// "fake":{
			// 	"firefox":">=100"
			// 	},
			// "__toto":
			// {
			// 	bw:{"firefox":">=500.0"},
			// 	fn:()=>
			// 	{
			// 		return false
			// 	}
			// }
		},

		ENGINES:
		{

			webkit:[

			]
		}
	}

	function BrowserHunter (ua)
	{
		if(typeof ua === "string" && ua !== "")
		{
			return BrowserHunterMain(ua)
		}

		if(typeof window.navigator !== "undefined" 
			&& typeof window.navigator.userAgent !== "undefined")
		{
			return (BrowserHunterMain(window.navigator.userAgent))
		}else
		{
			throw new Error('only from web environment');
		}
	}


	const BrowserHunterMain = function(UA)
	{
		const Build           = BrowserHunterBuild
		const {util, ...Auth} = BrowserHunterAuth(UA)
		let scores            = {inferency:[], inconsistency:[]}
		
		Auth.updater		  = [] // from Auth::byFeatures, used to update features list
		Auth.updaterVer 	  = ""
		Auth.spoofedBy        = false // 'incosistency' or 'feature' or 'env'
		Auth.spoof            = false
		Auth.uaEmpty          = false
		Auth.uaFrame          = false // used to check inconsistency
		Auth.UA               = '' // working ua
		Auth.uaRaw            = UA    // initial ua
		Auth.scores           = {}

		// window.toto = 4
		// const bName = "firefox"
		// const bVersion = "106"
		// window.InstallTrigger = 45
		// window.chrome = {webstore:true}
		// window.navigator = {requestMIDIAccess:true}
		// (Auth.uaEmpty && Auth.rebuild.found > 1 && (Auth.spoofedBy = "feature"))

		if(typeof window !== "undefined" && typeof window.document !== "undefined")
		{
			// preliminary spoof check: inconsistencies (vacuity, difference...)
			Auth.byInconsistency()

			Auth.received = Build(Auth.UA)

			// trying to identify by features
			Auth.suggested = Build(false, Auth.rebuildAttempt());

			// secondary spoof check: features
			(typeof Auth.suggested.bw !== "undefined" & Auth.suggested.bw.name !== Auth.received.bw.name && (Auth.spoofedBy = "feature"))

			Auth.SKEL = Auth.spoof ? Auth.suggested : Auth.received

console.log(Auth.suggested, Auth.received)

			if(!Auth.uaEmpty)
			{
				Auth.updaterVer = Auth.SKEL.bw.v
			}

			// trying to authentificate by features
			// scores.inferency = Auth.byFeatures(
			// {
				// able to force specific features
				// ruleSet:["navigator.requestMIDIAccess"]
			// })

			// Auth.isMobile()

		}else
		{
			Auth.SKEL = Build(Auth.uaRaw)

			// Node environment : Used for tests
			if(typeof process !== "undefined" && typeof process.release !== "undefined")
			{
				// console.log("No window", Auth.uaRaw)
				// console.log(process.release.name) //.search(/node|io.js/) !== -1)

			}else{

				// or that's means the client has no window Environment
				Auth.spoofedBy = "env"
				Auth.spoof = true
			}

		}

		function is(options)
		{
			// x.is({
			// 	bot:true, 
			// 	bw:true, 
			// 	hw:true, 
			// 	mob:true, 
			// 	cog:true, 
			// 	os:"windows",
			// 	phone:true,
			// 	tablet:true,
			// 	tv:true,
			// })
		}

		return {
			SKEL      :Auth.SKEL,
			spoof     :Auth.spoof,
			spoofedBy :Auth.spoofedBy,
			uaEmpty   :Auth.uaEmpty,
			updater   :Auth.updater,
			updaterVer:Auth.updaterVer,
			scores
		}
	}

	function BrowserHunterBuild(_UA, _rebuild=false)
	{
		if(getCaller(new Error().stack) !== "browserhuntermain")
			throw ("[Parse] can only be used from BrowserHunterMain")

		function getCaller(e)
		{
			let re = /(\w+)@|at (\w+) \(/g; re.exec(e);
			let aRegexResult = re.exec(e);
			return (aRegexResult?.[1] || aRegexResult?.[2])?.toLowerCase() || '';
		}

        let osTarget
		let skel = // for skeleton
		{
			bot  : { name: undefined },
			os   : { name: undefined, v: undefined, arch: undefined },
			bw   : { name: undefined, v: undefined, fake: false },
			bweng: { name: undefined, v: undefined },
			hw   : { name: undefined },
			mob  : false,
			cog  : false
		}

		const merge = function(...objects)
		{
			const isObject = obj => obj && typeof obj === 'object';
			// (obj+"") === "[object Object]"

			return objects.reduce((prev, obj) => 
			{
				Object.keys(obj).forEach(key => {
					const pVal = prev[key];
					const oVal = obj[key];

					if (Array.isArray(pVal) && Array.isArray(oVal)) {
						prev[key] = pVal.concat(...oVal);
					}
					else if (isObject(pVal) && isObject(oVal)) {
						prev[key] = merge(pVal, oVal);
					}
					else {
						prev[key] = oVal;
					}
				});

				return prev;
			}, {});
		}

		if(_rebuild)
		{
			return merge(skel, _rebuild)
		}



		function fallbackHints()
		{

			// Some browsers explicitly hide navigator.userAgentData from being read. See e.g. Brave PR 11932.
			// https://github.com/brave/brave-core/pull/11932

			// const isChromium = brand => brand.brand === "Chromium";
			// console.log(!!globalThis.navigator?.userAgentData?.brands?.some(isChromium));
			// navigator.userAgentData?.brands?.some(b => b.brand === 'Google Chrome')

		}



		const parse = (function()
		{
			getHacks()
			getBot()
			getOs()
			getBrowser()
		})()

		//     ____        __
		//    / __ )____  / /_
		//   / __  / __ \/ __/
		//  / /_/ / /_/ / /_
		// /_____/\____/\__/
		
		// According to the list at http://www.useragentstring.com/pages/useragentstring.php?typ=Crawler with 442 user agent strings listed as bots:

		// 208 user agent strings of bots contains the word "bot"
		// 63 user agent strings of bots contains the word "crawl"
		// 37 user agent strings of bots contains the word "spider"
		// 282 user agent strings of bots contains either "bot", "crawl" or "spider"


		// 31% Search Engines (i.e. the "good" bots)
		// 5% Scrapers
		// 4.5% Hacking tools
		// 0.5% Spammers
		// 20.5% Other Impersonators (basically all other non-humans)


		// usage with wget
		// http://www.useragentstring.com/?uas=Opera/9.70%20(Linux%20i686%20;%20U;%20en-us)%20Presto/2.2.0&getText=all


		function getHacks()
		{
			// (\$\{|(\\\x[0-9]+){1,}|\?>|(?:select.+(?:from)?.+|from.+(?:where)?|OR 1=1|\' or 1 --|;--\' AND)|password)
			// console.log(new RegExp("(\\$\\{|(\\\\x[0-9]+){1,}|\\?>|(?:select.+(?:from)?.+|from.+(?:where)?|OR 1=1|\\' or 1 --)|password)", 'i').test(_UA))
		}

		function getBot ()
		{
        	const occPre = _UA.match(/(bot[_\-\s/);]{1,}|( bot )|bot\.(?:html|php))/i)
    		const occNormalized = _UA.match(/([\w-]+(?<!ro)bot(?:[\w-]+)?)(?:\/([\d.]+)?)?(?:[(\w\s;]+(http))?/i)

			if (occPre !== null)
			{
			    if (occNormalized !== null)
			    {
			    	////////////////
			        // has bot occ

			        skel.bot = { name: occNormalized[1].toLowerCase(), v: occNormalized[2] }
			        skel.bot.type = deepFindCallback(RSC.BOT, cbBotByType, skel.bot.name)
			    }
			    else
			    {
			    	///////////////////////
			        // has bot/ver pattern

			        skel.bot = getBotCustom()
			        if (skel.bot.name !== undefined)
			        {
			            skel.bot.type = deepFindCallback(RSC.BOT, cbBotByType, skel.bot.name)
			        }
			        // ELSE: console.log('bot is not listed')
			    }

			}else
			{
				///////////////////
			    // custom pattern

			    skel.bot = getBotCustom()
			    if (skel.bot !== null)
			    {
			        skel.bot.type = (typeof skel.bot.name !== "undefined")
			        	? deepFindCallback(RSC.BOT, cbBotByType, skel.bot.name)
			        	: undefined
			    }
			    else // is not a bot
			    {
			        skel.bot.name = undefined
			    }
			}


			//     __  ___________    ____  __    ________________
			//    / / / / ____/   |  / __ \/ /   / ____/ ___/ ___/
			//   / /_/ / __/ / /| | / / / / /   / __/  \__ \\__ \
			//  / __  / /___/ ___ |/ /_/ /  , /___/ /___ ___/ /__/ /
			// /_/ /_/_____/_/  |_/_____/_____/_____//____/____/
			
			if(typeof window !== "undefined" && 
				(window?.navigator ?? false) && 
				window.navigator?.webdriver === true)
			{
				console.log("BOOOOT : webdriver, headless chrome")
				skel.bot.name = "Headless"
			}
		}



        // try hard all which "containing" bot keyword
        function getBotCustom() 
        {
        	const custom = [].concat.apply([], Object.values(RSC.BOT))
        	.map(x => // each bot category
        	{
        		// flaten the rewrite rule object to form a valid regexp
        		return (typeof (x) !== 'string')
        			? Object.keys(x).join('|') // [0]
        			: x
        	}) 
        	// check for: (contain bot in name / not finish by bot)
        	.filter(x => x.match(/(?<=(?:[\w-]+))(?:bot)(?=(?:[\w-]+))|.*(?<!bot)$/i)).join('|')


	        // check for name
			const reCustom = new RegExp(custom, 'i')
			const rx = _UA.match(reCustom)

			// check for version
			let v
			if (rx !== null)
			{
				// try to apply the write rule (multi names bot cases)
				rx[0] = deepFindCallback(RSC.BOT, cbBotByName, rx[0])

				// extract version if exists
			    const reV = new RegExp(`${rx[0]}/([0-9.]+)`, 'i')
			    v = _UA.match(reV)
			    v = v === null ? undefined : v[1]
			}
			else
			{
				// console.warn('Bot custom not listed')
			    v = undefined
			}
			return { name: rx !== null ? rx[0].toLowerCase() : undefined, v }
        }


        function cbBotByType(obj, key, botName)
        {
			const match = ((!(obj[key] instanceof Array) ? Object.values(obj[key]) : obj[key]).filter((o) =>
			{
				return typeof (o) !== 'string'
					// if multi key based rules {matchkey0:botname, matchkey1:botname...}
					// botname, ... botname... 
					? [...Object.keys(o), ...Object.values(o)].map(oo => oo.toLowerCase()).includes(botName.toLowerCase())
					// or direct named key
					: o.toLowerCase() === botName.toLowerCase()

			})).shift()
			return match !== undefined ? key : false
        }

        function cbBotByName(obj, key, botName)
        {
			const match = ((!(obj[key] instanceof Array) ? Object.values(obj[key]) : obj[key]).filter((o) =>
			{
				// if multi key based rules {matchkey0:botname, matchkey1:botname...}
				// matchkey0... matchkey1
				return (typeof (o) !== 'string' ? Object.keys(o) : [o]).map(oo => oo.toLowerCase()).includes(botName.toLowerCase())
			})).shift()

			return match !== undefined
		        ? typeof match !== 'string'
			    	? Object.values(match)[0] // check for rewrite rule
			    	: botName.toLowerCase() // or hold actual found name
		        : false
        }

        //    ____
        //   / __ \_____
        //  / / / / ___/
        // / /_/ (__  )
        // \____/____/

        function getOs()
        {
			if (skel.bot.name === undefined)
			{
			    osTarget = Object.keys(Object.entries(RSC.OS)

			    	// match against UA string
			    	.map(o=>{o[1].mr = o[1].m(_UA); return o})

			    	// sort matches result
					.sort(([, a], [, b]) => (b.mr === null ? 0 : b.mr.length) - (a.mr === null ? 0 : a.mr.length))

					// exclude unmatchable
					.reduce((r, [k, v]) => ({ ...r, [k]: (v.mr === null ? 0 : v.mr.length) }), {})
				).shift() // get the one with most matches

		        const os = RSC.OS[osTarget]
		        let matches = os.mr

		        if (matches !== null)
		        {
		        	// match entry point
		        	matches = cleanMatch(matches)
					if (matches[os.p.e] !== undefined)
					{
						skel.os.name = ({}.propertyIsEnumerable.call(os, 'n') ? os.n : matches[os.p.n]).toLowerCase()
						skel.os.v = {}.propertyIsEnumerable.call(os.p, 'v') && (matches.length - 1) >= os.p.v
						    ? matches[os.p.v].replaceAll('_', '.')
						    : undefined

						// Win64; IA64	System has a 64-bit processor (Intel).
						// Win64; x64	System has a 64-bit processor (AMD).
						// WOW64	A 32-bit version of Internet Explorer is running on a 64-bit processor.
						skel.os.device = {}.propertyIsEnumerable.call(os.p, 'd') ? matches[os.p.d] : undefined
						skel.os.arch = osTarget !== 'mac'
							// get specified arch
						    ? {}.propertyIsEnumerable.call(os, 'a') ? os.a : matches[os.p.a]
						    // For mac -> matching from given pos
						    : (matches.length - 1) >= os.p.a
						    	? (matches[os.p.a].toLowerCase().split(/\s/)[0] !== 'ppc')
						        	? (skel.os.v >= 11.4)
						        		? 'm1'
						                : 'x86_64'
						        	: 'ppc64'
						        : 'x86_64'

						// check for regular os names, version based
						let osVerNames = RSC.OSVERSION?.[osTarget] ?? undefined
						osVerNames = osVerNames ? Object.fromEntries(osVerNames) : osVerNames
						if(osVerNames !== undefined)
						{
							if (osTarget === 'mac' && skel.os.v === undefined)
							{
								// if ({}.propertyIsEnumerable.call(osVerNames, matches[0]))
								skel.os.v = osVerNames?.[matches[0]] ?? undefined
							}

							if (osTarget === 'dos')
							{
								skel.os.v = osVerNames?.[skel.os.v] ?? undefined
								if (skel.os.v instanceof Object) //['6.3', { d: '8.1', a: 'RT(8.1)' }],
								{
									skel.os.v = skel.os.arch === 'arm'
										? skel.os.v.a // get name for ARM version (from arch)
										: skel.os.v.d // or regular name
								}
							}
						}

						if (osTarget === 'nix')
						{
							// match distribution name (and version)
						    let match
						    switch (skel.os.name)
						    {
						        case 'linux':
						            match = _UA.match(new RegExp(RSC.DISTRIB, 'i'))
						            if (match !== null)
						            {
						                let distFmt = { 'linux mint': '[/)( ]?(\\w*)' }
						                const distName = match[1].toLowerCase()
						                const verPattern = Object.keys(distFmt).includes(distName)
						                    ? distFmt[distName]
										    : '(?:/([0-9.-a-z]+))?' // generic

						                const ver = _UA.match(new RegExp(distName + verPattern, 'i'))
						                skel.os.vv = (ver !== null && ver[1] !== '') ? ver[1] : undefined
						            }
						            break

						        default:
						            skel.os.name = skel.os.name.match(/bsd/i) ? 'bsd' : skel.os.name
						            match = _UA.match(/(freebsd|dragonfly|netbsd|openbsd)/i)
						            break
						    }
						    skel.os.v = (match !== null && match[1] !== '') ? match[1].toLowerCase() : undefined
						}

					}
		        }
			}
        }

        //     ____
        //    / __ )_________ _      __________  _____
        //   / __  / ___/ __ \ | /| / / ___/ _ \/ ___/
        //  / /_/ / /  / /_/ / |/ |/ (__  )  __/ /
        // /_____/_/   \____/|__/|__/____/\___/_/

        function getBrowser()
        {
        	if (skel.bot.name !== undefined)
			{
				return false
			}
			// [
			// 	bName 		'Maxthon',
			// 	osTargets	[AND, DOS], 
			// 	osDisallow	[], 
			// 	rule 		'(?:maxthon \\(([0-9.]+)\\);|mxbrowser/([0-9.]+))',
			//	ruleVer		[['7.0','nameX'], ['8.0','nameY']]
			// ],
			for (let [, [bName, osTargets, osDisallow, rule, verRule]] of RSC.BROWSERS.entries())
			{
				// check for allowed targets
				if (osTargets.some(os => [ALL, osTarget].includes(os)))
				{
					// if it's target ALL, ensure osTarget allowed too
					if (!osDisallow.includes(osTarget))
					{
			    		let match = _UA.match(new RegExp(rule, 'i'))
			    		if (match !== null)
			    		{
			                match = cleanMatch(match)
							verRule = typeof verRule !== "undefined"
								? Object.fromEntries(verRule)
								: undefined

			                skel.bw.name = bName.toLowerCase()
			                skel.bw.v = (verRule
			                	? verRule?.[match[0]] ?? match[0] // cross version (eg.: 6.1 refer to windows 7...)
			                	: match[0]).toLowerCase() // regular one

			                getEngine()
			    		}
					}
				}
			}
        }

		function getEngine()
		{
			// engines by match score
		    let [engName, engMatches] = Object.entries(RSC.ENGINES).map(e => [e[0], _UA.match(new RegExp(e.pop(), 'i'))])
		        .sort(([, a], [, b]) => (b === null ? 0 : b.length) - (a === null ? 0 : a.length)).shift()
		    if (engMatches !== null)
		    {
		        engMatches = cleanMatch(engMatches)
		        skel.bweng.name = engName
		        skel.bweng.v = engMatches.pop()
		    }else
		    {
		    	// console.log("eng not found, fallback", RSC.ENGINES)
		    }
		}

		//    __  ____  _ __
		//   / / / / /_(_) /____
		//  / / / / __/ / / ___/
		// / /_/ / /_/ / (__  )
		// \____/\__/_/_/____/
		
        function cleanMatch(matches)
		{
			const m = matches.filter(m => m !== undefined).map(m => m.toLowerCase())
			return m.length === 1 ? m : m.slice(1) // shift the first global matching string
		}

		function deepFindCallback (obj, matcher, needle)
		{
			// console.log("\n\n>>>> enter", needle)
		    for (const key in obj)
		    {
		    	// console.log("iteration", needle)
		        if (typeof obj[key] === 'object')
		        {
		        	const m = matcher(obj, key, needle)
		        	// console.log("after avance", key, needle, m)
		            if (!m)
		            {
		            	// console.log("recursion", m, needle)
		            	deepFindCallback(obj[key], matcher, needle)
		            }
		            else
		            {
		            	// console.log("key found", m)
		            	return m
		            }
		        }
		        // else
		        // {
		        // 	console.log("is not array", key, obj[key])
		        // }
		    }
		}


		// let output = Object.setPrototypeOf({ua}, {
		// 	haveFun:()=>true
		// })
		// console.log(Object.getOwnPropertyNames(output), Reflect.ownKeys(output.__proto__))
		return {
			...skel
		}
	}

    function BrowserHunterAuth()
    {
		if(getCaller(new Error().stack) !== "browserhuntermain")
			throw ("\x1b[35m[Auth] can only be used from BrowserHunterMain\x1b[0m")

		function getCaller(e)
		{
			let re = /(\w+)@|at (\w+) \(/g; re.exec(e);
			let aRegexResult = re.exec(e);
			return (aRegexResult?.[1] || aRegexResult?.[2])?.toLowerCase() || '';
		}



		function featureExec(_test, _SKEL)
		{
			let statement
			if(typeof _test === "string") // from feature path
			{
				const props = _test.split('.')
				statement = props.reduce((parent, child) =>
				{
					return parent && (typeof parent[child] !== "undefined" ? parent[child] : undefined)
				}, window)
				statement = statement !== undefined

			}else
			{
				// is a function rule
				statement = _test(_SKEL)
			}
			return statement
		}

		function rebuildAttempt()
		{
			let SKEL = {}
			let foundOs = []
			let bw = {
				isCandidate:{},
				versions:[]
			}

			SKEL.bweng = { name: byEngine() }
			SKEL.mob = isMobile()

			Object.keys(INF).forEach((mode)=>{

				switch(mode.toLowerCase())
				{
					case "browsers_discover": 
					{
						Object.entries(INF[mode]).forEach((f)=>
						{
							let [browserName, test] = f;
							bw.isCandidate[browserName] = test() || undefined
						})
						// ΛV
						console.log(bw.isCandidate)
					}
					break;

					case "browsers_lock":
					{
						// cleanup inert candidate
						bw.isCandidate = Object.fromEntries(Object.entries(bw.isCandidate).filter(([k,v])=>v))
						const bwNames = Object.keys(bw.isCandidate)
						if(bwNames.length === 1)
						{
							let browser = bwNames[0]
							// add the version found during discovery
							bw.versions.push(bw.isCandidate[browser])
							SKEL.bw     = { name:browser }
							console.log("yes", browser);
						}

						Object.entries(INF[mode]).forEach((f)=>
						{
							let specs;

							// for version matcher (for suggested)
							specs = rebuildSpecs(f, SKEL.bw.name, SKEL)
							if(Object.keys(specs.rules).includes(SKEL.bw.name) && specs.isValidFeature)
							{
								// console.log(feature, versions)
								bw.versions.push(specs.versions)
							}


							// for updater (from received)
							specs = rebuildSpecs(f, this.received.bw.name, this.received, true)
							if(this.received.bw.v !== undefined && Object.keys(specs.rules).includes(this.received.bw.name))
							{
								specs.isValidFeature = specs.isValidFeature()

								let isValidVersion = vcmp(this.received.bw.v, specs.versions, specs.feature)
								let bypassRule       = false
								let bypassValidation = false
								let isValidSpecial   = false
								if(specs.hasSpecial)
								{
									if("trustedOS" in specs.hasSpecial)
									{
										if(!specs.hasSpecial.trustedOS.includes(this.received.os.name))
										{
											if((!specs.isValidFeature && isValidVersion))
											{
												// example:
													// opera (same version) on Linux has no g_opr, but it has it on windows,
													// so, since the trusted OS is not matching the current OS, it can be treated as 'foreign'
													// Note, that INF.BROWSERS is not designed to strictly match OS's
												
												bypassValidation = true
											}

											if(specs.isValidFeature && isValidVersion)
											{
												// here, it fulfills all conditions, but on untrusted OS, 
												// then it can't be treated as 'target'. The rule fails
												bypassRule = true
												this.spoofedBy = "os"
											}
										}
									}
								}

								if(!bypassValidation)
								{
									///////////////////////////
									// for update guard viewer
									// e.g: currentBW is 109, ruleBW is 106 (>= 106, but an invalid statement of rule means that it's time to update ruleBW.
									this.updater.push({
										feature       : specs.feature, 
										versions      : specs.versions, 
										isValidVersion: (!bypassRule ? isValidVersion : false), 
										isValidFeature: specs.isValidFeature
									})
								}
							}
							
						})

						if(bw.versions.length)
						{
							// pop out the most recent version
							let min = 0
							let range = []
							let hasMax = false
							// versions = [">=3", ">=6", "<=22", [">=15", "<=20"], ">=45"]
							for(var v of bw.versions)
							{
								if(Array.isArray(v)) // has 
								{
									hasMax = v[1].split(/([<=>])/).filter(e=>!isNaN(e)&&e!=='').pop()
									v = v[0]
								}
								var split        = v.split(/([<=>])/)
								var [opt0, opt1] = split.filter(e=>isNaN(e))
								var [operands]   = split.filter(e=>!isNaN(e)&&e!=='')

								if(opt1 === undefined && opt0 === "=")
								{
									min = parseInt(operands) // featured only a this rev
							       	break;
								}else
								{
									// it remaining static or moves forward
									min = parseInt((opt0 === ">")
										? (min <= operands) 
											? (opt1 === "=")
												? operands
												: parseInt(operands)+1
											: min
										: min)
								}
								
								// reset range if greater rev is found
								hasMax = min > hasMax ? false : hasMax
							    console.log(min, hasMax)
							}
							SKEL.bw.v = [min, (hasMax ? '-'+hasMax:'')].filter(e=>e).join('-')
						}
							
						

					} break;
				}

			})
			return SKEL
		}

		function rebuildSpecs(_f, _bName, _SKEL, _seqExec)
		{
			_seqExec = _seqExec || false
			let isValidFeature   = false
			let [feature, setup] = _f;

			// declarative or functional test
			const testType = new RegExp("^__", 'i').test(feature)
			let rules      = testType ? setup.bw : setup
			let test       = testType ? setup.fn : feature								

			// EXCECUTE FEATURE by RULE TYPE

			isValidFeature = _seqExec ? ()=>featureExec(test, _SKEL) : featureExec(test, _SKEL)

			let hasSpecial = false
			let rule       = rules[_bName]
			let versions   = rule
			// ruleDefs[feature] = conditions

			// has special condition
			if(Array.isArray(rule))
			{
				versions   = rule.filter(v=>typeof v === "string")
				versions   = versions.length === 1 ? versions[0] : versions
				hasSpecial = rule.filter(v=>(typeof v !== "string"))
				hasSpecial = hasSpecial[0]
			}

			return {rules, versions, feature, isValidFeature, hasSpecial}
		}

		function byInconsistency()
		{
			if(typeof window.document !== "undefined" && document.body !== null)
			{
				// frame
				const frame = document.createElement("iframe");
				frame.setAttribute("sandbox", "allow-same-origin")
				frame.src = "javascript:false"
				frame.classList.add("hidden")
				document.body.appendChild(frame);
				this.uaFrame = frame.contentWindow.navigator.userAgent.trim()
				frame.remove()

				if(this.uaRaw !== this.uaFrame && this.uaFrame !== "") // UA differ
				{
					Object.defineProperty(this, "spoofedBy", {  // will be overwriting at rebuild checking
						configurable: false, 
						writable    : false, 
						value       :"inconsistency" 
					});
					this.spoof = true
				}

				if(this.uaFrame === "") // UA empty
				{
					this.spoofedBy = "inconsistency" // could (and allowed to) be overwriting at features checking
					this.uaEmpty = true
					this.spoof = true
				}

				// this.spoofedBy = this.spoof ? "inconsistency" : false
				this.UA = this.uaFrame
			}
		}

		function byEngine()
		{
			// ╔═══════════════════════╦══════════════════╦═══════════════════════╗
			// ║        Chrome         ║ Rendering Engine ║   JavaScript Engine   ║
			// ╠═══════════════════════╬══════════════════╬═══════════════════════╣
			// ║ iOS                   ║ iOS WebKit       ║ Nitro(JavaScriptCore) ║
			// ║ Android/Windows/Linux ║ Blink            ║ V8                    ║
			// ╚═══════════════════════╩══════════════════╩═══════════════════════╝

			let engine = undefined

			// https://caniuse.com/mdn-css_at-rules_supports_selector
			if("WebkitAppearance" in document.documentElement.style || // chrome * , safari >= 3, opera >= 14
				(typeof CSS !== "undefined" && typeof CSS.supports !== "undefined" && CSS.supports(`selector(input[type='time']::-webkit-calendar-picker-indicator)`))
			){
				// selector()
				// chrome >=83
				// safari >=14.1
				// edge >=83
				// firefox >=69
				// opera >=69
				// safari mobile >=14.5
				// samsung >=13

				// https://caniuse.com/mdn-css_selectors_past ???

				engine = "webkit"
				if(typeof window.chrome !== "undefined" || (typeof Intl !== "undefined" && typeof Intl.v8BreakIterator !== "undefined"))
				{
					engine = "blink"
				}
			}

			if("MozAppearance" in document.documentElement.style || // all
				typeof window.mozInnerScreenX === "number") // >=3.6
			{
				engine = "gecko"
				
				// registerContentHandler ????
			}

			if(typeof window.opera !== "undefined" && window.opera.toString() === "[object opera]")
			{
				engine = "presto"
			}

			if(/*@cc_on!@*/false)
			{
				engine = "trident"
			}

			return engine
		}

		function byUAString()
		{
			// opera -> 'OPR/x.x' >= 15
		}

		function byOs()
		{
			// >>>>>>>> WINDOWS
			// {"g_opr":{ // WINDOWS ONLY


			// >>>>>>> IOS
				// "__Webkit-touch-callout" ---> ios
				// "__Webkit-overflow-scrolling":

				// for chrome based
				// https://softwareengineering.stackexchange.com/questions/198375/why-is-it-impossible-for-google-to-port-v8-along-with-chromes-codebase-in-c-obj
				// if(window.chrome && !Intl.v8BreakIterator)
				// 	-> its ios or mac


			// >>>>>>>>>>> MAC
				// "__Moz-osx-font-smoothing":  --> mac
				// {
				// 	bw:{
				// 		"chrome":">=5",
				// 		"safari":">=4",
				// 		"edge":">=79",
				// 		"firefox":">=25",
				// 		"opera":">=15"
				// 	},
				// 	fn:()=>
				// 	{
				// 		return ((typeof CSS !== "undefined" && typeof CSS.supports !== "undefined") &&
				// 			CSS.supports("-moz-osx-font-smoothing", "auto"))
				// 	}
				// }
		}

		function byFeatures(_options)
		{

			let ruleDefs       = {}
			let isValidRule    = false
			let isValidVersion = false
			const bName        = this.SKEL.bw.name
			const bVersion     = this.SKEL.bw.v
			const bOs          = this.SKEL.os.name
			const ruleSet      = _options.ruleSet || false
			let scores =  {
				foreign:{ total:0, match:[] },
				target :{ total:0, match:{} },
				fake   :{ total:0, match:{} }
			}

			Object.entries(INF.BROWSERS).forEach((f, o) => 
			{
				// look for rules
				let fulfillName
				let [feature, setup] = f
				const bwRules        = new RegExp("^__", 'i').test(feature) ? setup.bw : setup

				// check for requested rules
				if(ruleSet && !ruleSet.includes(feature))
				{
					return false
				}

				// is a property rule
				isValidRule = featureExec(feature, setup, this.SKEL)

				// if the given BW has rule to match the feature
				if(Object.keys(bwRules).includes(bName))
				{
					let hasSpecial    = false
					let conditions    = bwRules[bName]
					let versions      = conditions
					ruleDefs[feature] = conditions

					// has special condition
					if(Array.isArray(conditions))
					{
						versions   = conditions.filter(v=>typeof v === "string")
						versions   = versions.length === 1 ? versions[0] : versions
						hasSpecial = conditions.filter(v=>(typeof v !== "string"))
						// special = Object.fromEntries(special.map(a=>Object.entries(a)[0]))
						hasSpecial = hasSpecial[0]
					}

					isValidVersion = vcmp(bVersion, versions, feature)

					let bypassRule       = false
					let bypassValidation = false
					let isValidSpecial   = false
					if(hasSpecial)
					{
						if("trustedOS" in hasSpecial)
						{
							if(!hasSpecial.trustedOS.includes(bOs))
							{
								if((!isValidRule && isValidVersion))
								{
									/* example:
										opera (same version) on Linux has no g_opr, but it has it on windows,
										so, since the trusted OS is not matching the current OS, it can be treated as 'foreign'
										Note, that INF.BROWSERS is not designed to strictly match OS's
									*/
									bypassValidation = true
								}

								if(isValidRule && isValidVersion)
								{
									// here, it fulfills all conditions, but on untrusted OS, 
									// then it can't be treated as 'target'. The rule fails
									bypassRule = true
									this.spoofedBy = "os"
								}
							}
						}
					}

					if(!bypassValidation)
					{
						///////////////////////////
						// for update guard viewer
						// e.g: currentBW is 109, ruleBW is 106 (>= 106, but an invalid statement of rule means that it's time to update ruleBW.
						this.updater.push({feature, versions, isValidVersion:(!bypassRule ? isValidVersion : false), isValidRule})


						let isNotFake = ((!isValidRule && !isValidVersion) || (isValidRule && isValidVersion))
						fulfillName = isNotFake 
							? (!bypassRule)
								? "target"
								: "fake"
							: "fake"

						scores[fulfillName].match[feature] = {rule:Boolean(isValidRule), v:isValidVersion, sp:isValidSpecial}
					}else
					{
						fulfillName = "foreign"
					}

				}else
				{
					fulfillName = "foreign"
					if(isValidRule)
					{
						scores[fulfillName].match.push(feature)
					}
				}

				scores[fulfillName].total++
			});

		  	// console.log(isValid, ruleDefs, `${score}/${fullScore}`)
		  	return {...scores, ruleDefs}
		}


		function isMobile()
		{
			let score = 0
			score = [
				 ((typeof screen !== "undefined" && screen?.availWidth < 400) ?? false),
				((typeof navigator != "undefined" && navigator?.maxTouchPoints > 1) ?? false),
				(
					( typeof window !== "undefined" && 
						(
							(typeof window.ontouchstart !== "undefined") || 
							(
								(typeof window.DocumentTouch !== "undefined" && typeof window.document !== "undefined") && 
								window.document instanceof window.DocumentTouch
							)
						) 
					)
				)
			].reduce((r, v)=> r+Number(Boolean(v)), 0)

			return Boolean(score >= 2)
		}


		function vcmp(vTarget, rule, ruleName)
		{

			const operatorDescriptor = function(current, target, name)
			{
				const operation = function(currentBrowserVersion, version)
				{
					if(!new RegExp('^([<>]?(?!~)[=~]?)([0-9a-z\.-]+(?<!\\.))$', 'i').test(version))
					{
						// console.warn(`%c[vcmp]: misspelled version rule: ${version}`, 'color: black; background: yellow; font-weight:bold; font-size:14px')
						console.warn(`\x1b[31m[vcmp]: misspelled version at rule[${name}]: ${version}\x1b[0m`)
						throw new Error(`\x1b[31m[vcmp]: misspelled version at rule[${name}]: ${version}\x1b[0m`);
					}

					let isLoose           = false;
					let expectedResults   = [0];
					let comparableVersion = currentBrowserVersion;
					// let currentBrowserVersion = o;

					if(typeof currentBrowserVersion !== 'string'){
						return void 0;
					}

				    if(version[0] === '>' || version[0] === '<'){
						comparableVersion = version.substr(1);
						if (version[1] === '=') {
							isLoose = true;
							comparableVersion = version.substr(2);
						} else {
							expectedResults = [];
						}
						if (version[0] === '>') {
							expectedResults.push(1);
						} else {
							expectedResults.push(-1);
						}
					} else if (version[0] === '=') {
						comparableVersion = version.substr(1);
					} else if (version[0] === '~') {
						isLoose = true;
						comparableVersion = version.substr(1);
					}

					return expectedResults.indexOf(
							comparator(currentBrowserVersion, comparableVersion, isLoose),
					) > -1
				}

				return Array.isArray(target)
					? operation(current, target[0], name) && operation(current, target[1], name)
					: operation(current, target, name)    
			}

			function getVersionPrecision(version) {
				return version.split('.').length;
			}

			function comparator(versionA, versionB, isLoose = false)
			{
				// 1) get common precision for both versions, for example for "10.0" and "9" it should be 2
				const versionAPrecision = getVersionPrecision(versionA);
				const versionBPrecision = getVersionPrecision(versionB);
				
				let precision           = Math.max(versionAPrecision, versionBPrecision);
				let lastPrecision       = 0;

				const chunks = [versionA, versionB].map((version) => 
				{
					const delta = precision - getVersionPrecision(version);

					// 2) "9" -> "9.0" (for precision = 2)
					const _version = version + new Array(delta + 1).join('.0');

					// 3) "9.0" -> ["000000000"", "000000009"]
					// return _version.split('.').map(chunk => new Array(20 - chunk.length).join('0') + chunk).reverse();
					return _version.split('.').map(chunk => chunk.padStart(23-chunk.length, '0')).reverse();
				});


				// adjust precision for loose comparison
				if (isLoose) {
					lastPrecision = precision - Math.min(versionAPrecision, versionBPrecision);
				}

				// iterate in reverse order by reversed chunks array
				precision -= 1;
				while (precision >= lastPrecision)
				{
					// console.log({precision}, {lastPrecision})
					// console.log({chunk0:chunks[0][precision], chunk1:chunks[1][precision]})

					// 4) compare: "000000009" > "000000010" = false (but "9" > "10" = true)
					if (chunks[0][precision] > chunks[1][precision]) {
						return 1;
					}

					if (chunks[0][precision] === chunks[1][precision])
					{
						if (precision === lastPrecision) {
							// all version chunks are same
							return 0;
						}
						precision -= 1;

					} else if (chunks[0][precision] < chunks[1][precision]) 
					{
						// console.log({precision}, {lastPrecision})
						// console.log({chunk0:chunks[0][precision], chunk1:chunks[1][precision]})
						return -1;
					}
				}
				return undefined;
			}

			return operatorDescriptor(vTarget, rule, ruleName)
		}

		return {

			rebuildAttempt,

			byInconsistency,
			byFeatures,
			
			isMobile,	    	
	    	util:{
	    		vcmp
	    	}
		}
    }


if(typeof window !== "undefined" && Object.keys(window).length > 0)
{
	// let br = BrowserHunter()
	// document.write("<br>"+JSON.stringify(br, null, 6).replace(/\n/g, '<br>').replace(/\s/g, '&nbsp;').replace(/"/g, '\''))

}else
{
	// let br = BrowserHunter(
	// 	// Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 YaBrowser/22.11.7.42 Yowser/2.5 Safari/537.36
	// 	// Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:102.0) Gecko/20100101 Firefox/102.0
	// 	// Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36
	// 	// Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:102.0) Gecko/20100101 Firefox/102.0
	// 	// Dalvik/1.6.0 (Linux; U; Android 4.0.3; ThinkPad Tablet Build/ThinkPadTablet_A400_03)
	// 	// "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"
	// 	// "Mozilla/5.0 (X11; U; Linux i686; es-ES; rv:2.0) Gecko/20100101 Linux Mint 16/Petra Firefox/25.0.1."
	// 	// "Mozilla/5.0 (compatible; CensysInspect/1.1; +https://about.censys.io/)"
	// 	// "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 OPR/92.0.0.0"
	// 	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 OPR/92.0.0.0"
	// )

	// // let util = require("util")
	// // console.log(util.inspect(BrowserHunterMain.constructor.__proto__, {showHidden: true, depth: null, colors: true, enumerable: false, compact:false }))
	// console.dir(br, {'maxArrayLength': null, depth:6})
}



	// check js env
	if(typeof(exports) !== "undefined")
	{
		console.log("EXPORTS")
		// node env
		if (require.main === module)
		{
			console.log("MAIN")
			// from cli
			var args = process.argv.slice(2)
			if(args.length > 0)
			{
				args.map(function(a, i)
				{
					console.log(BrowserHunter(a))
				    // console.log(i, a);
				})
			}

		}
		// from caller
		if(typeof module !== "undefined" && module.exports){
			exports = module.exports = BrowserHunter
			console.log("MODULE")
		}
		exports.BrowserHunter = BrowserHunter

	}else
	{
		console.log("EXPORTS")
		// requirejs env
		if(typeof(define) !== "undefined" && define.amd){
			console.log("DEFINE")
			define(function(){
				return BrowserHunter;
			})
		}else if(typeof window !== "undefined"){
			console.log("WINDOW")
			window.BrowserHunter = BrowserHunter
		}
	}

    // jQuery/Zepto specific (optional)
    // Note:
    //   In AMD env the global scope should be kept clean, but jQuery is an exception.
    //   jQuery always exports to global scope, unless jQuery.noConflict(true) is used,
    //   and we should catch that.
    // var $ = typeof window !== "undefined" && (window.jQuery || window.Zepto);
    // if ($ && !$.ua) {
    //     var parser = new UAParser();
    //     $.ua = parser.getResult();
    //     $.ua.get = function () {
    //         return parser.getUA();
    //     };
    //     $.ua.set = function (ua) {
    //         parser.setUA(ua);
    //         var result = parser.getResult();
    //         for (var prop in result) {
    //             $.ua[prop] = result[prop];
    //         }
    //     };
    // }
})(typeof window === "object" ? window : this);

