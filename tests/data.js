
exports.Browsers = [

//         __                                 ____  _____
//   _____/ /_  _________  ____ ___  ___     / __ \/ ___/
//  / ___/ __ \/ ___/ __ \/ __ `__ \/ _ \   / / / /\__ \
// / /__/ / / / /  / /_/ / / / / / /  __/  / /_/ /___/ /
// \___/_/ /_/_/   \____/_/ /_/ /_/\___/   \____//____/


/// ??????
{
	ua:"Mozilla/5.0 (Linux; Android 7.1.1; Samsung Chromebook 3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3529.0 Safari/537.36",
	osName  :"chromeos",
	osVer   :"",
	osArch  :"",
	bwName  :"chrome",
	bwVer   :"70.0.3529.0",
	bwEng   :"blink",
	bwEngVer:"70.0.3529.0"
},


{
	ua:"Mozilla/5.0 (X11; CrOS i686 4319.74.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36",
	osName  :"chromeos",
	osVer   :"4319.74.0",
	osArch  :"i686",
	bwName  :"chrome",
	bwVer   :"29.0.1547.57",
	bwEng   :"blink",
	bwEngVer:"29.0.1547.57"
},



//    ____  ______  __
//   / __ \/ ___/ |/ /
//  / / / /\__ \|   /
// / /_/ /___/ /   |
// \____//____/_/|_|


// m1
{
	ua:"Mozilla/5.0 (Macintosh; Intel Mac OS X 13_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.35",
	osName  :"mac",
	osVer   :"13.0",
	osArch  :"m1",
	bwName  :"edge",
	bwVer   :"107.0.1418.35",
	bwEng   :"blink",
	bwEngVer:"107.0.0.0"
},

// yandex
{
	ua:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 YaBrowser/22.11.7.42 Yowser/2.5 Safari/537.36",
	osName  :"mac",
	osVer   :"10.15.7",
	osArch  :"x86_64",
	bwName  :"yandex browser",
	bwVer   :"22.11.7.42",
	bwEng   :"blink",
	bwEngVer:"108.0.0.0"
},

// firefox
{
	ua:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:102.0) Gecko/20100101 Firefox/102.0",
	osName  :"mac",
	osVer   :"10.13",
	osArch  :"x86_64",
	bwName  :"firefox",
	bwVer   :"102.0",
	bwEng   :"gecko",
	bwEngVer:"20100101"
},

// brave
{
	ua:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Brave Chrome/83.0.4103.116 Safari/537.36",
	osName  :"mac",
	osVer   :"10.14.6",
	osArch  :"x86_64",
	bwName  :"brave",
	bwVer   :"???",
	bwEng   :"blink",
	bwEngVer:"83.0.4103.116"
},

//  _       ___           __
// | |     / (_)___  ____/ /___ _      _______
// | | /| / / / __ \/ __  / __ \ | /| / / ___/
// | |/ |/ / / / / / /_/ / /_/ / |/ |/ (__  )
// |__/|__/_/_/ /_/\__,_/\____/|__/|__/____/


	{
		ua:"Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 6.3; .NET CLR 3.2.14471; Win64; x64)",
		osName  :"windows",
		osVer   :"8.1",
		osArch  :"win64",
		bwName: "ie",
		bwVer: "9.0"
	},

	{
		ua:"Mozilla/5.0 (compatible; MSIE 7.0; Windows 98; Win 9x 4.90; Trident/5.0)",
		osName  :"windows",
		osVer   :"me",
		osArch  :"win32",
		bwName: "ie",
		bwVer: "9.0",
		bwEng: "trident",
		bwEngVer: "5.0"
	},

	// CE
	{
		ua:"Mozilla/4.0 (compatible; MSIE 6.0; Windows CE; IEMobile 7.11)",
		osName  :"windows mobile",
		osVer   :"ce",
		osArch  :"arm",
		bwName: "ie mobile",
		bwVer: "7.11",
	},

	// RT 8.1
	{
		ua:"Mozilla/5.0 (Windows NT 6.3; ARM; Trident/7.0; Touch; ARMBJS; rv:11.0) like Gecko",
		osName  :"windows",
		osVer   :"rt(8.1)",
		osArch  :"arm",
		bwName: "ie",
		bwVer: "11.0",
		bwEng: "trident",
		bwEngVer: "7.0"
	},

	// opera
	{
		ua:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0",
		osName  :"windows",
		osVer   :"10",
		osArch  :"win64",
		bwName  :"opera",
		bwVer   :"94.0.0.0",
		bwEng   :"blink",
		bwEngVer:"108.0.0.0"
	},

	// chromium
		{
			ua:"Mozilla/5.0 (Windows NT 10.0; Win64; x64; Chromium GOST) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36",
			osName  :"windows",
			osVer   :"10",
			osArch  :"win64",
			bwName  :"chromium",
			bwVer   :"gost",
			bwEng   :"blink",
			bwEngVer:"108.0.0.0"
		},
		{
			ua:"Mozilla/5.0 (Windows NT 10.0; Win64; x64; Xbox; Xbox One) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.67",
			osName  :"windows",
			osVer   :"10",
			osArch  :"win64",
			bwName  :"edge",
			bwVer   :"99.0.1150.67",
			bwEng   :"blink",
			bwEngVer:"99.0.4844.74",
			device: "xbox one"
		},

	// brave ?????
		{
			ua:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) brave/0.8.3 Chrome/49.0.2623.108 Brave/0.37.3 Safari/537.36",
			osName  :"windows",
			osVer   :"10",
			osArch  :"win64",
			bwName  :"brave",
			bwVer   :"0.8.3",
			bwEng   :"blink",
			bwEngVer:"49.0.2623.108"
		},
		{
			ua:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36 Brave/1.7.92",
			osName  :"windows",
			osVer   :"10",
			osArch  :"win64",
			bwName  :"brave",
			bwVer   :"1.7.92",
			bwEng   :"blink",
			bwEngVer:"81.0.4044.122"
		},

	// maxthon
	{
		ua:"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36 Maxthon/5.2.3.4000",
		osName  :"windows",
		osVer   :"10",
		osArch  :"wow64",
		bwName  :"maxthon",
		bwVer   :"5.2.3.4000",
		bwEng   :"blink",
		bwEngVer:"61.0.3163.79"
	},
	{
		ua:"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; MAXTHON)",
		osName  :"windows",
		osVer   :"vista",
		osArch  :"wow64",
		bwName  :"maxthon",
		bwEng   :"trident",
		bwEngVer:"5.0"
	},
	{
		ua:"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.12 (KHTML, like Gecko) Maxthon/3.4.1.1000 Chrome/18.0.966.0 Safari/535.12",
		osName  :"windows",
		osVer   :"vista",
		bwName  :"maxthon",
		bwVer: "3.4.1.1000",
		bwEng   :"webkit",
		bwEngVer:"535.12"
	},

	// Sleipnir
	{
		ua:"Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 10.0; WOW64; Trident/7.0; Sleipnir6/6.4.4; SleipnirSiteUpdates/6.4.4)",
		osName  :"windows",
		osVer   :"10",
		osArch  :"wow64",
		bwName  :"sleipnir",
		bwVer   :"6.4.4",
		bwEng   :"trident",
		bwEngVer:"7.0"
	},

	// sputnik browser
	{
		ua:"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2236.0 SputnikBrowser/1.9.76.3 Safari/537.36",
		osName  :"windows",
		osVer   :"xp",
		bwName  :"sputnikbrowser",
		bwVer   :"1.9.76.3",
		bwEng   :"blink",
		bwEngVer:"41.0.2236.0"
	},

	// baidu browser
	{
		ua:"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 BIDUBrowser/2.x Safari/537.31",
		osName  :"windows",
		osVer   :"7",
		bwName  :"baidu browser",
		bwVer   :"2.x",
		bwEng   :"webkit",
		bwEngVer:"537.31"
	},
	// baidu spark
	{
		ua:"Baidu Spark Browser 2.x, Chrome 33.0.1",
		bwName  :"baidu spark",
		bwVer   :"2.x",
	},


	// edge webview
	{
		ua:"Mozilla/5.0 (Windows NT 10.0; WOW64; MSAppHost/3.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393",
		osName  :"windows",
		osVer   :"10",
		osArch: "wow64",
		bwName  :"edge webview",
		bwVer   :"3.0",
		bwEng   :"blink",
		bwEngVer:"51.0.2704.79"
	},

	//         __                              ____  _____
	//   _____/ /_  _________  ____ ___  ___  / __ \/ ___/
	//  / ___/ __ \/ ___/ __ \/ __ `__ \/ _ \/ / / /\__ \
	// / /__/ / / / /  / /_/ / / / / / /  __/ /_/ /___/ /
	// \___/_/ /_/_/   \____/_/ /_/ /_/\___/\____//____/

	{
		ua:'Mozilla/5.0 (X11; CrOS aarch64 15054.114.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
		osName  :"chromeos",
		osVer   :"15054.114.0",
		osArch  :"aarch64",
		bwName  :"chrome",
		bwVer   :"106.0.0.0",
		bwEng   :"blink",
		bwEngVer:"106.0.0.0"
	},
	{
		ua:'Mozilla/5.0 (X11; U; CrOS i686 0.9.128; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.341 Safari/534.10',
		osName  :"chromeos",
		osVer   :"0.9.128",
		osArch  :"i686",
		bwName  :"chrome",
		bwVer   :"8.0.552.341",
		bwEng   :"webkit",
		bwEngVer:"534.10"
	},


	//    _____             ____  _____
	//   / ___/__  ______  / __ \/ ___/
	//   \__ \/ / / / __ \/ / / /\__ \
	//  ___/ / /_/ / / / / /_/ /___/ /
	// /____/\__,_/_/ /_/\____//____/

	{
		ua:"Mozilla/5.0 (X11; U; SunOS sun4m) AppleWebKit/531.76.39 (KHTML, like Gecko) Chrome/56.3.8811.4183 Safari/534.47",
		osName  :"solaris",
		osVer: "sun4m",
		osArch: "sparc",
		bwName  :"chrome",
		bwVer   :"56.3.8811.4183",
		bwEng   :"webkit",
		bwEngVer:"531.76.39"
	},



//                           ____ _____ ____
//   ____  ____  ___  ____  / __ ) ___// __ \
//  / __ \/ __ \/ _ \/ __ \/ __  \__ \/ / / /
// / /_/ / /_/ /  __/ / / / /_/ /__/ / /_/ /
// \____/ .___/\___/_/ /_/_____/____/_____/
//     /_/

	{
		ua:'Mozilla/5.0 (X11; OpenBSD i386; rv:36.0) Gecko/20100101 Firefox/36.0 SeaMonkey/21F6DD',
		osName  :"bsd",
		osVer: "openbsd",
		osArch  :"i386",
		bwName  :"seamonkey",
		bwVer   :"21f6dd",
		bwEng   :"gecko",
		bwEngVer:"20100101"
	},
	{
		ua:'Mozilla/5.0 (X11; OpenBSD 6.6; x86_64; rv:72.0) Gecko/20100101 Firefox/72.0',
		osName  :"bsd",
		osVer: "openbsd",
		osVerSb: "6.6",
		osArch  :"x86_64",
		bwName  :"firefox",
		bwVer   :"72.0",
		bwEng   :"gecko",
		bwEngVer:"20100101"
	},
	{
		ua:'Wget/1.20.3 (openbsd6.9)',
		osName:"bsd",
		osVer:"openbsd"
	},

//     __    _
//    / /   (_)___  __  ___  __
//   / /   / / __ \/ / / / |/_/
//  / /___/ / / / / /_/ />  <
// /_____/_/_/ /_/\__,_/_/|_|


	// slackware
	{
		ua:"Mozilla/5.0 (X11; U; Slackware Linux x86_64; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.30 Safari/532.5",
		osName  :"linux",
		osVer: "slackware",
		osArch  :"x86_64",
		bwName  :"chrome",
		bwVer   :"4.0.249.30",
		bwEng   :"webkit",
		bwEngVer:"532.5"
	},

	// Linux Mint
		{
			ua:"Mozilla/5.0 (X11; U; Linux i686; es-ES; rv:2.0) Gecko/20100101 Linux Mint 16/Petra Firefox/25.0.1.",
			osName  :"linux",
			osVer   :"linux mint",
			osVerSb :"16",
			osArch  :"i686",
			bwName  :"firefox",
			bwVer   :"25.0.1.",
			bwEng   :"gecko",
			bwEngVer:"20100101"
		},

		{
			ua:"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.11) Gecko/2009060308 Linux Mint/7 (Gloria) Firefox/3.0.11",
			osName  :"linux",
			osVer: "linux mint",
			osVerSb: "7",
			osArch  :"i686",
			bwName  :"firefox",
			bwVer   :"3.0.11",
			bwEng   :"gecko",
			bwEngVer:"2009060308"
		},
		{
			ua:"Opera/9.80 (X11; Linux i686; Edition Linux Mint) Presto/2.12.388 Version/12.16",
			osName  :"linux",
			osVer: "linux mint",
			osArch  :"i686",
			bwName  :"opera",
			bwVer   :"12.16",
			bwEng   :"presto",
			bwEngVer:"2.12.388"
		},

	// fedora
	{
		ua:"Mozilla/5.0 (X11; U; Linux x86_64; ru-RU; rv:1.8.1.16) Gecko/20080716 Fedora/1.1.11-1.fc9 SeaMonkey/1.1.11",
		osName  :"linux",
		osVer: "fedora",
		osVerSb: "1.1.11-1.fc9",
		osArch  :"x86_64",
		bwName  :"seamonkey",
		bwVer   :"1.1.11",
		bwEng   :"gecko",
		bwEngVer:"20080716"
	},

	// chromium
	{
		ua:'Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/99.0.4852.96 Chrome/99.0.4852.96 Safari/537.36',
		osName  :"linux",
		osVer: "ubuntu",
		osArch  :"x86_64",
		bwName  :"chromium",
		bwVer   :"99.0.4852.96",
		bwEng   :"blink",
		bwEngVer:"99.0.4852.96"
	},

	// ubuntu
		{
			ua:'Mozilla/5.0 (X11; U; Linux ppc64; en-US; rv:1.8.1.14) Gecko/20080418 Ubuntu/7.10 (gutsy) Firefox/2.0.0.14',
			osName  :"linux",
			osVer: "ubuntu",
			osVerSb:"7.10",
			osArch  :"ppc64",
			bwName  :"firefox",
			bwVer   :"2.0.0.14",
			bwEng   :"gecko",
			bwEngVer:"20080418"
		},
		{
			ua:'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:14.0) Gecko/20100101 Firefox/14.0.1',
			osName  :"linux",
			osVer: "ubuntu",
			osArch  :"x86_64",
			bwName  :"firefox",
			bwVer   :"14.0.1",
			bwEng   :"gecko",
			bwEngVer:"20100101"
		},


	// iceweasel
	{
		ua:'Mozilla/5.0 (X11; debian; Linux x86_64; rv:15.0) Gecko/20100101 Iceweasel/15.0',
		osName  :"linux",
		osVer: "debian",
		osArch  :"x86_64",
		bwName  :"iceweasel",
		bwVer   :"15.0",
		bwEng   :"gecko",
		bwEngVer:"20100101"
	},

	// pale moon
	{
		ua:"Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Goanna/4.8 Firefox/68.0 PaleMoon/29.4.6",
		osName  :"linux",
		osArch  :"x86_64",
		bwName  :"palemoon",
		bwVer   :"29.4.6",
		bwEng   :"goanna",
		bwEngVer:"4.8"
	},

	// vivaldi
	{
		ua:"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.122 Safari/537.36 Vivaldi/2.3.1440.60",
		osName  :"linux",
		osArch  :"x86_64",
		bwName  :"vivaldi",
		bwVer   :"2.3.1440.60",
		bwEng   :"blink",
		bwEngVer:"72.0.3626.122"
	},

	// Arora
		{
			ua:"Mozilla/5.0 (Windows; U; ; en-US) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3) Arora/0.8.0",
			osName  :"windows",
			bwName  :"arora",
			bwVer   :"0.8.0",
			bwEng   :"webkit",
			bwEngVer:"527"
		},
		{
			ua:"Mozilla/5.0 (Windows; U; Windows NT 6.0; de-DE) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3)  Arora/0.4 (Change:  )",
			osName  :"windows",
			osVer: "vista",
			bwName  :"arora",
			bwVer   :"0.4",
			bwEng   :"webkit",
			bwEngVer:"527"
		},


	// midori
		{
			ua:"Mozilla/5.0 (X11; U; Linux x86_64; ru-ru) AppleWebKit/525.1+ (KHTML, like Gecko, Safari/525.1+) midori 622",
			osName  :"linux",
			osArch  :"x86_64",
			bwName  :"midori",
			bwVer   :"622",
			bwEng   :"webkit",
			bwEngVer:"525.1"
		},
		{
			ua:"Linux x86_64; U; X11; en-gb Gecko/20190101 Midori/107.00000",
			osName  :"linux",
			osArch  :"x86_64",
			bwName  :"midori",
			bwVer   :"107.00000",
			bwEng   :"gecko",
			bwEngVer:"20190101"
		},
		{
			ua:"Mozilla/5.0 (Windows NT 6.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Midori/10.0.2 Chrome/98.0.4758.141 Electron/17.4.10 Safari/537.36",
			osName  :"windows",
			osVer: "vista",
			osArch  :"win64",
			bwName  :"midori",
			bwVer   :"10.0.2",
			bwEng   :"blink",
			bwEngVer:"98.0.4758.141"
		},
		{
			ua:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Midori/10.0.2 Chrome/98.0.4758.141 Electron/17.4.10 Safari/537.36",
			osName  :"mac",
			osVer: "10.15.7",
			osArch  :"x86_64",
			bwName  :"midori",
			bwVer   :"10.0.2",
			bwEng   :"blink",
			bwEngVer:"98.0.4758.141"
		},


//            _           __                      ____  __
//  _      __(_)___  ____/ /___ _      _______   / __ \/ /_  ____  ____  ___
// | | /| / / / __ \/ __  / __ \ | /| / / ___/  / /_/ / __ \/ __ \/ __ \/ _ \
// | |/ |/ / / / / / /_/ / /_/ / |/ |/ (__  )  / ____/ / / / /_/ / / / /  __/
// |__/|__/_/_/ /_/\__,_/\____/|__/|__/____/  /_/   /_/ /_/\____/_/ /_/\___/


	{
		ua:"Mozilla/5.0 (Windows Mobile 10; Android 10.0; Microsoft; Lumia 950XL) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36 Edge/40.15254.603",
		osName: "windows phone",
		osVer:"10",
		bwName:"edgehtml",
		bwVer: "40.15254.603",
		bwEng:"edgehtml",
		bwEngVer:"40.15254.603"
	},

	{
		ua:"Mozilla/5.0 (Windows Phone 10.0; Android 6.0.1; Microsoft; Lumia 650 Dual SIM) Gecko/20100101 Firefox/68.0",
		osName: "windows phone",
		osVer:"10.0",
		bwName:"firefox",
		bwVer: "68.0",
		bwEng:"gecko",
		bwEngVer:"20100101"
	},

	{
		ua:"Mozilla/4.0 (compatible; MSIE 7.0; Windows Phone OS 7.0; Trident/3.1; IEMobile/7.0; HTC; HTC6995LVW)",
		osName: "windows phone",
		osVer:"7.0",
		bwName:"ie mobile",
		bwVer: "7.0",
		bwEng:"trident",
		bwEngVer:"3.1"
	},


//     _ ____  _____
//    (_) __ \/ ___/
//   / / / / /\__ \
//  / / /_/ /___/ /
// /_/\____//____/


	// ????
	{
		ua:"Mozilla/5.0 (iPhone; CPU iPhone OS 15_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 PrivaBrowser-iOS/0.75 Version/75 Safari/605.1.15",
		osName: "ios",
		osVer:"15.6",
		bwName:"PrivaBrowser-iOS",
		bwVer: "0.75",
		bwEng:"webkit",
		bwEngVer:"605.1.15",
		device:"iphone"
	},

	// chrome
		{
			ua:"Mozilla/5.0 (iPhone; CPU iPhone OS 16_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/105.0.5195.129 Mobile/15E148 Safari/604.1",
			osName: "ios",
			osVer:"16.0",
			bwName:"chrome",
			bwVer: "105.0.5195.129",
			bwEng:"webkit",
			bwEngVer:"605.1.15",
			device:"iphone"
		},

		{
			ua:"Mozilla/5.0 (iPad; CPU OS 14_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/87.0.4280.163 version/2.8 Mobile/15E148 Safari/604.1",
			osName: "ios",
			osVer:"14.5",
			bwName:"chrome",
			bwVer: "87.0.4280.163",
			bwEng:"webkit",
			bwEngVer:"605.1.15",
			device:"ipad"
		},

	// edge
	{
		ua:"Mozilla/5.0 (iPhone; CPU iPhone OS 16_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.0 EdgiOS/108.1462.77 Mobile/15E148 Safari/605.1.15",
		osName: "ios",
		osVer:"16.3",
		bwName:"edge",
		bwVer: "108.1462.77",
		bwEng:"webkit",
		bwEngVer:"605.1.15",
		device:"iphone"
	},

	// maxthon
	{
		ua:'Mozilla/5.0 (iPhone; CPU iPhone OS 15_0_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/10.0 Mobile/15E148 Safari/602.1 MXiOS/6.0.3.139',
		osName: "ios",
		osVer:"15.0.1",
		bwName:"maxthon",
		bwVer: "6.0.3.139",
		bwEng:"webkit",
		bwEngVer:"605.1.15",
		device:"iphone"
	},

	// firefox
	{
		ua:'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_2 like Mac OS X) AppleWebKit/603.2.4 (KHTML, like Gecko) FxiOS/7.5b3349 Mobile/14F89 Safari/603.2.4',
		osName: "ios",
		osVer:"10.3.2",
		bwName:"firefox",
		bwVer: "7.5b3349",
		bwEng:"webkit",
		bwEngVer:"603.2.4",
		device:"iphone"	
	},

	// safari mobile
	{
		ua:'Mozilla/5.082584686 Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1',
		osName: "ios",
		osVer:"11.4.1",
		bwName:"safari mobile",
		bwVer: "11.0",
		bwEng:"webkit",
		bwEngVer:"605.1.15",
		device:"iphone"	
	},
	{
		ua:'Mozilla/5.0 (iPhone; CPU iPhone OS 11_1_1 like Mac OS X) AppleWebKit/604.3.5 (KHTML, like Gecko) Version/11.0 Mobile/15B150 Safari/604.1 BMID/E6797E1BCB',
		osName: "ios",
		osVer:"11.1.1",
		bwName:"safari mobile",
		bwVer: "11.0",
		bwEng:"webkit",
		bwEngVer:"604.3.5",
		device:"iphone"	
	},

	// opera
		{
			ua:'Mozilla/5.0 (iPhone; CPU iPhone OS 15_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) OPiOS/16.0.15.124050 Mobile/15E148 Safari/9537.53',
			osName: "ios",
			osVer:"15.6",
			bwName:"opera mobile",
			bwVer: "16.0.15.124050",
			bwEng:"webkit",
			bwEngVer:"605.1.15",
			device:"iphone"	
		},
		{
			ua:'Opera/9.80 (iPad; Opera Mini/7.0.5/191.283; U; es) Presto/2.12.423 Version/12.16',
			osName: "ios",
			bwName:"opera mini",
			bwVer: "7.0.5",
			bwEng:"presto",
			bwEngVer:"2.12.423",
			device:"ipad"	
		},

	// baidu browser
	{
		ua:'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.24 T5/2.0 baidubrowser/6.1.1.0',
		osName: "ios",
		osVer: "10.2.1",
		bwName:"baidu browser",
		bwVer: "6.1.1.0",
		bwEng:"webkit",
		bwEngVer:"602.4.6",
		device:"iphone"	
	},
	// baidu hd
	{
		ua:'Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) BaiduHD/5.4.0.0 Mobile/10A406 Safari/8536.25',
		osName: "ios",
		osVer: "9.1",
		bwName:"baidu hd",
		bwVer: "5.4.0.0",
		bwEng:"webkit", 
		bwEngVer:"534.46",
		device:"ipad"	
	},



//     ___              __           _     __
//    /   |  ____  ____/ /________  (_)___/ /
//   / /| | / __ \/ __  / ___/ __ \/ / __  /
//  / ___ |/ / / / /_/ / /  / /_/ / / /_/ /
// /_/  |_/_/ /_/\__,_/_/   \____/_/\__,_/


	// webview
	{
		ua:"Mozilla/5.0 (Linux; Android 12; PEGM10 Build/RKQ1.211103.002; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/98.0.4758.101 Mobile Safari/537.36",
		osName: "android",
		osVer:"12",
		bwName:"android webview",
		bwVer: "98.0.4758.101",
		bwEng:"blink",
		bwEngVer:"98.0.4758.101"
	},


	// rockmelt
	{
		ua:'Mozilla/5.0 (Linux; U; Android 4.2.2; ru-ru; ZTE V809 Build/JDQ39) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 Rockmelt/1.0.4.41 (com.rockmelt.socialreader)',
		osName: "android",
		osVer:"4.2.2",
		bwName:"rockmelt",
		bwVer: "1.0.4.41",
		bwEng:"webkit",
		bwEngVer:"534.30"
	},

	// baidu box app
	{
		ua:'Mozilla/5.0 (Linux; Android 10; JEF-AN00 Build/HUAWEIJEF-AN00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/97.0.4692.98 Mobile Safari/537.36 T7/13.26 BDOS/1.0 (HarmonyOS 2.2.0) SP-engine/2.64.0 baiduboxapp/13.26.0.10 (Baidu; P1 10) dumedia/7.39.71.13',
		osName: "android",
		osVer:"10",
		bwName:"baidu boxapp",
		bwVer: "13.26.0.10",
		bwEng:"blink",
		bwEngVer:"97.0.4692.98"
	},
	
    
	// QQ

		{
			ua:'Mozilla/5.0 (Linux; U; Android 8.1.0; zh-cn; NX611J Build/OPM1.171019.011) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/66.0.3359.126 MQQBrowser/10.3 Mobile Safari/537.36',
			osName: "android",
			osVer:"8.1.0",
			bwName:"qqbrowser",
			bwVer: "10.3",
			bwEng:"blink",
			bwEngVer:"66.0.3359.126"
		},
		{
			ua:'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.3427.400 QQBrowser/9.6.12449.400',
			osName: "windows",
			osVer:"7",
			bwName:"qqbrowser",
			bwVer: "9.6.12449.400",
			bwEng:"blink",
			bwEngVer:"53.0.2785.104"
		},
		{
			ua:'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.2 Safari/605.1.15 QQBrowserLite/1.2.9',
			osName: "mac",
			osVer:"10.13.6",
			osArch: "x86_64",
			bwName:"qqbrowser",
			bwVer: "1.2.9",
			bwEng:"webkit",
			bwEngVer:"605.1.15"
		},

	// DuckDuckGo
	{
		ua:'Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/109.0.5414.117 Mobile DuckDuckGo/103 Safari/537.36',
		osName: "android",
		osVer:"10",
		bwName:"duckduckgo",
		bwVer: "103",
		bwEng:"blink",
		bwEngVer:"109.0.5414.117"
	},

	// Jio
	{
		ua:'Mozilla/5.0 (Linux; Android 11; RMX2061) AppleWebKit/537.36 (KHTML, like Gecko) JioPages/4.0 Chrome/101.0.4951.41 Mobile Safari/537.36',
		osName: "android",
		osVer:"11",
		bwName:"jio browser",
		bwVer: "4.0",
		bwEng:"blink",
		bwEngVer:"101.0.4951.41"
	},

	// SurfBrowser
	{
		ua:'Mozilla/5.0 (Android 10; samsung SM-M515F) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36 SurfBrowser/3.0',
		osName: "android",
		osVer:"10",
		bwName:"surf browser",
		bwVer: "3.0",
		bwEng:"blink",
		bwEngVer:"30.0.0.0"
	},

	// VenusBrowser
	{
		ua:'Mozilla/5.0 (Linux; Android 11; 211033MI Build/RP1A.200720.011) AppleWebKit/603.36 (KHTML, like Gecko) VenusBrowser/3.2.14 Chrome/108.0.4692.98 Mobile Safari/537.36',
		osName: "android",
		osVer:"11",
		bwName:"venus browser",
		bwVer: "3.2.14",
		bwEng:"webkit",
		bwEngVer:"603.36"
	},
	// NTENTBrowser
	{
		ua:'Mozilla/5.0 X11 (Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.9 NTENTBrowser/4.2.0.184 Safari/537.36',
		osName: "linux",
		osArch: "x86_64",
		bwName:"ntent browser",
		bwVer: "4.2.0.184",
		bwEng:"blink",
		bwEngVer:"54.0.2840.9"
	},


	// uc browser
	{
		ua:'Linux; U; Android 4.4.2; en-US; HM NOTE 1W Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 UCBrowser/11.0.5.850 U3/0.8.0 Mobile Safari/534.30',
		osName: "android",
		osVer:"4.4.2",
		bwName:"uc browser",
		bwVer: "11.0.5.850",
		bwEng:"webkit",
		bwEngVer:"534.30"
	},
	// Yandex
	{
		ua:"Mozilla/5.0 (Linux; arm_64; Android 13; SM-A536E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 YaBrowser/23.1.0.24.00 (alpha) SA/3 Mobile Safari/537.36",
		osName: "android",
		osVer:"13",
		bwName:"yandex browser",
		bwVer: "23.1.0.24.00",
		bwEng:"blink",
		bwEngVer:"108.0.0.0"
	},

	// webview
	{
		ua:"Mozilla/5.0 (Linux; Android 10; SM-G980F Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/78.0.3904.96 Mobile Safari/537.36",
		osName: "android",
		osVer:"10",
		bwName:"android webview",
		bwVer: "78.0.3904.96",
		bwEng:"blink",
		bwEngVer:"78.0.3904.96"
	},

	// Samsung
		{
			ua:"Mozilla/5.0 (Linux; Android 10; SAMSUNG SM-G965F) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/14.0 Chrome/87.0.4280.141 Mobile Safari/537.36",
			osName: "android",
			osVer:"10",
			bwName:"samsung browser",
			bwVer: "14.0",
			bwEng:"blink",
			bwEngVer:"87.0.4280.141"
		},
		{
			ua:"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/14.0 Chrome/87.0.4280.141 Safari/537.36",
			osName: "android",
			osVer:undefined,
			bwName:"samsung browser",
			bwVer: "14.0",
			bwEng:"blink",
			bwEngVer:"87.0.4280.141"
		},

	// edge
	{
		ua:"Mozilla/5.0 (Linux; Android 12; M2101K9G) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36 EdgA/108.0.1462.48",
		osName: "android",
		osVer:"12",
		bwName:"edge",
		bwVer: "108.0.1462.48",
		bwEng:"blink",
		bwEngVer:"108.0.0.0"
	},


	// kai os
	{
		ua:"Mozilla/5.0 (Mobile; LYF/F220B/LYF-F220B-003-01-45-051119;Android; rv:48.0) Gecko/48.0 Firefox/48.0 KAIOS/2.5",
		osName: "android",
		osVer:"kaios/2.5",
		bwName:"firefox",
		bwVer: "48.0",
		bwEng:"gecko",
		bwEngVer:"48.0",
	},

	// xiaomi
	{
		ua:"Mozilla/5.0 (Linux; U; Android 6.0.1; zh-cn; Redmi 4A Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/79.0.3945.147 Mobile Safari/537.36 XiaoMi/MiuiBrowser/14.5.12",
		osName: "android",
		osVer:"6.0.1",
		bwName:"miui browser",
		bwVer: "14.5.12",
		bwEng:"blink",
		bwEngVer:"79.0.3945.147",
	},

	// yandex
	{
		ua:"Mozilla/5.0 (Linux; Android 6.0.1; MI NOTE LTE Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 YaBrowser/17.10.2.145.00 Mobile Safari/537.36",
		osName: "android",
		osVer:"6.0.1",
		bwName:"yandex browser",
		bwVer: "17.10.2.145.00",
		bwEng:"blink",
		bwEngVer:"61.0.3163.100",
	},

	// fake !
	{
		ua:"Mozilla/5.0 (Linux; Android 9; ASUS_X00TD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.5304.91 Mobile Safari/537.36 OPR/73.0.3788.68491",
		osName: "android",
		osVer:"9",
		bwName:"opera mobile",
		bwVer: "73.0.3788.68491",
		bwEng:"blink",
		bwEngVer:"107.0.5304.91"
	},

	// opera touch
	{
		ua:"Mozilla/5.0 (Linux; Android 8.1.0; ZB602KL Build/OPM1) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/69.0.3497.100 Mobile Safari/537.36 OPT/1.10.36",
		osName: "android",
		osVer:"8.1.0",
		bwName:"opera touch",
		bwVer: "1.10.36",
		bwEng:"blink",
		bwEngVer:"69.0.3497.100"
	},

	//opera mini
	{
		ua:"Opera/9.80 (Android; Opera Mini/70/93.110; U; ru) Presto/2.12.423 Version/12.16",
		osName: "android",
		bwName:"opera mini",
		bwVer: "70",
		bwEng:"presto",
		bwEngVer:"2.12.423"
	},

	// huawei
	{
		ua:"Mozilla/5.0 (Linux; Android 12; HarmonyOS; ANA-AN00; HMSCore 6.9.0.302) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.88 HuaweiBrowser/13.0.1.301 Mobile Safari/537.36",
		osName: "android",
		osVer:"12",
		bwName:"huawei browser",
		bwVer: "13.0.1.301",
		bwEng:"blink",
		bwEngVer:"99.0.4844.88"
	},

	// maxthon
	{
		ua:"Mozilla/5.0 (Linux; U; Android 2.2; fr-fr; TOSHIBA_FOLIO_AND_A Build/TOSHIBA_FOLIO_AND_A) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1; Maxthon (4.0.4.1000);",
		osName: "android",
		osVer:"2.2",
		bwName:"maxthon",
		bwVer: "4.0.4.1000",
		bwEng:"webkit",
		bwEngVer:"533.1"
	},

	{
		ua:"Mozilla/5.0 (Linux; Android 4.4.2; 6043D Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36 MxBrowser/4.5.7.1000",
		osName: "android",
		osVer:"4.4.2",
		bwName:"maxthon",
		bwVer: "4.5.7.1000",
		bwEng:"blink",
		bwEngVer:"30.0.0.0"
	},


// Brand/Model


// Mozilla/5.0 (Linux; Android 8.0.0; RNE-L21 Build/HUAWEIRNE-L21; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/101.0.4951.41 Mobile Safari/537.36 Midori/6
// Mozilla/5.0 (Linux; Android 7.1.2; Aquaris U Plus Build/NJH47F; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/71.0.3578.99 Mobile Safari/537.36 Midori/6

// Bot
{
	ua     :"Mozilla/5.0 (compatible; CensysInspect/1.1; +https://about.censys.io/)",
	botName: "censysinspect",
	botVer : "1.1",
	botType:"security"
},
{
	ua     :"Mozilla/5.0 (compatible; NetcraftSurveyAgent/1.0; +info@netcraft.com)",
	botName: "netcraftsurveyagent",
	botVer : "1.0",
	botType:"security"
},
{
	ua     :"Mozilla/4.0 (compatible; Netcraft Web Server Survey)",
	botName: "netcraftsurveyagent",
	botType:"security"
},
{
	ua     :"SEOlizer/1.1 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13 (+https://www.seolizer.de/bot.html)",
	botVer : "1.1",
	botName: "seolizer",
	botType:"seo"
},
{
	ua     :"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-GB; rv:1.0; trendictionbot0.5.0; trendiction search; http://www.trendiction.de/bot; please let us know of any problems; web at trendiction.com) Gecko/20071127 Firefox/3.0.0.11",
	botVer : "0.5.0",
	botName: "trendictionbot",
	botType:"market"
},

{
	ua     :"Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Nicecrawler/1.1; +http://www.nicecrawler.com/) Chrome/90.0.4430.97 Safari/537.36",
	botName: "nicecrawler",
	botVer : "1.1",
	botType:"archives"
},

{
	ua     :"Mozilla/5.0 (compatible; tcinet.ru/Bot)",
	botName: "tcinet.ru",
	botType:"domain"
},

{
	ua     :"QuerySeekerSpider ( http://queryseeker.com/bot.html )",
	botName: "queryseekerspider",
	botType:"market"
},



{
	ua     :"Mozilla\/5.0 (compatible; Baiduspider\/2.0; +http:\/\/www.baidu.com\/search\/spider.html)",
	botName: "baiduspider",
	botVer: "2.0",
	botType:"serp"
},

{
	ua     :"Mozilla\/5.0 (Linux;u;Android 4.2.2;zh-cn;) AppleWebKit\/534.46 (KHTML,like Gecko) Version\/5.1 Mobile Safari\/10600.6.3 (compatible; Baiduspider\/2.0; +http:\/\/www.baidu.com\/search\/spider.html)",
	botName: "baiduspider",
	botVer: "2.0",
	botType:"serp"
},



{
	ua:"Mozilla/5.0 (compatible; coccocbot-web/1.0; +http://help.coccoc.com/searchengine",
	botName: "coccocbot-web",
	botVer : "1.0",
	botType:"serp"	
},
{
	ua:"Mozilla/5.0 (compatible; RSSMicro.com RSS/Atom Feed Robot)",
	botName: "rssmicro",
	botType:"rss"	
},
{
	ua:"Mozilla/5.0 (compatible; RSSMicro.com RSS/Atom Feed Robot)",
	botName: "rssmicro",
	botType:"rss"	
},
{
	ua:"masscan\/1.0 (https:\/\/github.com\/robertdavidgraham\/masscan)",
	botName: "masscan",
	botVer: "1.0",
	botType:"parasite"	
},



//                 _          __
//  _      _____  (_)________/ /
// | | /| / / _ \/ / ___/ __  /
// | |/ |/ /  __/ / /  / /_/ /
// |__/|__/\___/_/_/   \__,_/


// microsoft is fakes ie11 UA to gains html5 android features in mobile app
// then this is an original windows phone
{
	ua:'Mozilla/5.0 (Mobile; Windows Phone 8.1; Android 4.0; ARM; Trident/7.0; Touch; rv:11.0; IEMobile/11.0; NOKIA; Lumia 930) like iPhone OS 7_0_3 Mac OS X AppleWebKit/537 (KHTML, like Gecko) Mobile Safari/537',
},

{
	// windows 9.2 never existed...
	ua:"Mozilla/5.0 (Windows NT 9_2; Win64; x64) AppleWebKit/582.39 (KHTML, like Gecko) Chrome/73.0.2223 Safari/537.36"
},

{
	ua:"Mozilla/5.0 (X11; Linux x188_64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.63 Safari/537.31"
},

// FAKE !
{
	// there is no chrome v359...
	ua:'Mozilla/5.0 (Linux; Android 9; ASUS_X00TD; Flow) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/359.0.0.288 Mobile Safari/537.36',
	osName: "android",
	osVer:"9",
	bwName:"flow",
	bwVer: "flow",
	bwEng:"blink",
	bwEngVer:"359.0.0.288",
	spoofedBy:"useragent"
},

]

