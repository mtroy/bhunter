exports.Logger = function()
{
	const c = 
	{
		Reset      : "\x1b[0m",
		Bright     : "\x1b[1m",
		Dim        : "\x1b[2m",
		Underscore : "\x1b[4m",
		Blink      : "\x1b[5m",
		Reverse    : "\x1b[7m",
		Hidden     : "\x1b[8m",

		fgBlack    : "\x1b[30m",
		fgRed      : "\x1b[38;5;124m",
		fgGreen    : "\x1b[38;5;82m",
		fgLime     : "\x1b[38;5;154m",
		fgYellow   : "\x1b[38;5;11m",
		fgOrange   : "\x1b[38;5;166m",
		fgBlue     : "\x1b[38;5;63m",
		fgMagenta  : "\x1b[38;5;91m",
		fgCyan     : "\x1b[38;5;81m",
		fgWhite    : "\x1b[37m",
		fgGray     : "\x1b[90m",

		bgBlack    : "\x1b[40m",
		bgRed      : "\x1b[48;5;124m",
		bgGreen    : "\x1b[42m",
		bgYellow   : "\x1b[43m",
		bgOrange   : "\x1b[48;5;214m",
		bgBlue     : "\x1b[44m",
		bgMagenta  : "\x1b[45m",
		bgCyan     : "\x1b[46m",
		bgWhite    : "\x1b[47m",
		bgGray     : "\x1b[100m"
	}

	const cts = {
		failed:c.bgRed+c.fgWhite,
		passed:c.bgGreen+c.fgLime,
		warn:c.bgOrange+c.fgOrange
	}

	return {
		l:(...d)=>
		{
			let output = []
			for(let i = 0; i < d.length; i++)
			{
				if(typeof d[i] === "string")
				{
					// let fm = d[i].match(new RegExp("^=\\[([a-z]+)\\]", 'i'))
					// if(fm)
					// {
					// 	d[i] = d[i].toLowerCase()
					// 	let [flag, flagString] = fm
					// 	d[i] = d[i].replace(flag, `${cts[flagString]}[${flagString}]`)
					// }

					fm = d[i].match(new RegExp("^\n\\[([a-z]+)\\]", 'i'))
					if(fm)
					{
						d[i] = d[i].toLowerCase()
						let [flag, flagString] = fm
						d[i] = d[i].replace(flag, `\n${cts[flagString]}[${flagString}]${c.Reset}`)
					}

					fm = d[i].match(new RegExp("« [a-z0-9_\\-\\.\\s]+ »", 'ig'))
					// console.log("fm", fm)

					if(fm)
					{
						for(mi = 0; mi < fm.length; mi++)
						{
							d[i] = d[i].replaceAll(fm[mi], `${c.fgOrange}${fm[mi]}${c.Reset}`)	
						}
					}
				}
				output.push(d[i])
			}
			console.log.apply(console, [...output])
		},
		rsl:
		{
			passed:"\x1b[38;5;82m"+
"    ____                           __\n"+
"   / __ \\____ ______________  ____/ /\tScore: %s / %s\n"+
"  / /_/ / __ `/ ___/ ___/ _ \\/ __  /\n"+
" / ____/ /_/ (__  |__  )  __/ /_/ /\n"+
"/_/    \\__,_/____/____/\\___/\\__,_/\n\x1b[0m",

			failed:"\x1b[38;5;124m\n"+
"    ______      _ __         __\n"+
"   / ____/___ _(\x1b[38;5;124m_) /__  ____/ /\tScore: %s / %s\n"+
"  / /_  / __ `/ / / _ \\/ __  /\n"+
" / __/ / /_/ / / /  __/ /_/ /\n"+
"/_/    \\__,_/_/_/\\___/\\__,_/\n\x1b[0m"


		}
	}
	// console.log("%c", "color: black; background: yellow; font-weight:bold; font-size:14px")
	// console.log("\x1b[31m test \x1b[0m")
}