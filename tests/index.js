
const fs = require('fs');
const readline = require('readline');
let rl = readline.createInterface(process.stdin, process.stdout); 

const {Logger} = require("./helper.js")
const {Browsers} = require("./data.js")
const BrowserHunter = require('./../src/BrowserHunter.js');
const {l, rsl} = Logger()



// l(Browsers.entries())
// for(let [, {ua, ...props}] of Browsers.entries())

let dump  = []
let score = 0
let tests = Browsers.length
for(let {ua, ...props} of Browsers)
{
	try
	{
		const bh  = BrowserHunter(ua).SKEL
		const err = (bh.bot.name === undefined
		? 
			[
				[bh.os.name, props.osName, "Os Name"],
				[bh.os.v, props.osVer, "Os Ver"],
				[bh.os.vv, props.osVerSb, "Os Ver Sub"],
				[bh.os.arch, props.osArch, "Os Arch"],
				[bh.bw.name, props.bwName, "Bw Name"],
				[bh.bw.v, props.bwVer, "Bw Ver"],
				[bh.bweng.name, props.bwEng, "Bw Engine"],
				[bh.bweng.v, props.bwEngVer, "Bw Engine Ver"],
				[bh.os.device, props.device, "Device"],
				[bh.spoofedBy, props.spoofedBy, "Spoofed By"],
			]
		: 
			[
				[bh.bot.name, props.botName, "Bot Name"],
				[bh.bot.v, props.botVer, "Bot Ver"],
				[bh.bot.type, props.botType, "Bot Ver Sub"],
			]
		).filter(a=>a[0]!==a[1])


		if(err.length)
		{
			l(`\n[warn]: ${ua}`, "\n"+err.map(p=>(`\t>>>> ${p[2]} « ${p[1]} » expected, but « ${p[0]} »`)).join("\r\n"))
			dump.push({
				ua,
				err:err.map(e=>({
						field: e[2], 
						expected: e[1],
						got: e[0] ?? null,
					})
				)
			})

		}else
		{
			l(`\n[passed]: ${ua}`)
			score++
		}

	}catch(e)
	{
		l("[failed]: "+e.toString())
	}
}

const result = score === tests ? "passed" : "failed"
l("=".repeat(process.stdout.columns))
l(rsl[result], score, tests)


rl.question("Dump warned tests to a log file ? y(es)/[no]: ", function(answer)
{
	if(/y(es)?/i.test(answer))
	{
		fs.mkdir('./tests/logs', { recursive: true }, (err) => {
			if (err) throw err;


			fs.writeFile("./tests/logs/errDump.json", JSON.stringify(dump, null, 4), function(err){
				if(err) throw err;

				console.log("Log file has been wrote under ./tests/logs/errDump.json");
			}); 
		});
	}
	rl.close();
})


// l("[warn]: toto")
// l("[passed]: toto")
// l("[failed]: toto")

// let br = BrowserHunter("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 OPR/92.0.0.0")
// console.dir(br, {'maxArrayLength': null, depth:6})

